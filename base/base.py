__author__ = 'Jeff.xie'
# encoding:utf-8
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as EC
from public.log import Log
import  glob

log = Log()
class Base():

    def __init__(self, driver):
        self.driver = driver

    def find_ele(self, loc, timeout=20, poll=0.5):
        # WebDriverWait(self.driver,timeout,poll).until(EC.presence_of_element_located(loc))
        try:
            ele=WebDriverWait(self.driver, timeout, poll).until(lambda x: x.find_element(*loc))
            log.info("{0}页面中找到{1}元素".format(self,loc))
            return ele
        except Exception as msg:
             log.error("{0}页面中未能找到{1}元素".format(self,loc))

    def find_ele_for_exception(self, loc, timeout=20, poll=0.5):
        return WebDriverWait(self.driver, timeout, poll).until(lambda x: x.find_element(*loc))

    def find_elements(self, loc, timeout=20, poll=0.5):
        return WebDriverWait(self.driver, timeout, poll).until(lambda x: x.find_elements(*loc))

    def click_ele(self, loc,sleep_time=0.2):
        self.find_ele(loc).click()
        sleep(sleep_time)

    def input_ele(self, loc, text):
        ele = self.find_ele(loc)
        ele.clear()
        ele.send_keys(text)

    # <option value="005">Credit Agricole Corporate and Investment Bank (005)</option>
    # input元素中的值需要通过这种方式来获取，ele.text不能获取input的值
    def get_ele_attribute_value(self,loc,attribute_value):  #获取元素中熟悉value的值 005
        return self.find_ele(loc).get_attribute(attribute_value).strip()

    def get_ele_attribute_text(self,loc):   #获取元素在浏览器显示的值 Credit Agricole Corporate and Investment Bank (005)
        return self.find_ele(loc).text.strip()


    def scroll_windows(self,js):
        self.driver.execute_script(js)

    def execute_js(self,js,ele):
        self.driver.execute_script(js, ele)   #js删除某个元素的属性,所以需要元素作为参数

    def switch_windows(self):
        current_window = self.driver.current_window_handle
        windows = self.driver.window_handles
        for i in windows:
            if current_window != i:
                self.driver.switch_to.window(i)

    def close_and_switch_window(self,first_window):
        self.driver.close()
        self.driver.switch_to.window(first_window)


    def SwitchWindow(self):
        windows = self._driver.window_handles
        self._driver.switch_to.window(windows[-1])
        self._driver.maximize_window()
        print("Switch to the latest window and wait")

    def select_a_ele_from_select(self, loc, value,sleep_time=0.1):
        ele=self.find_ele(loc)
        ele.click()
        sleep(sleep_time)
        Select(ele).select_by_visible_text(value)

    # <option value="005">Credit Agricole Corporate and Investment Bank (005)</option>
    def select_a_ele_from_select_by_flag(self, loc, flag, value,sleep_time=0.1):
        ele = self.find_ele( loc)
        ele.click()
        sleep(sleep_time)
        if "value" in flag:   #获取元素中熟悉value的值 005
            Select(ele).select_by_value(value)
        elif "text" in flag:  #获取元素在页面显示的值
            Select(ele).select_by_visible_text(value)
        elif "id" in flag:
            Select(ele).select_by_index(value)

    def picturereading(self,path):    #bookneme输入图片名字方便在图片目录中找到对应的图片
        # image = glob.glob(path + '*.png')
        image = glob.glob(path)
        return image

    def switch_frame(self, loc):
        """
        多表单嵌套切换
        :param loc: 传元素的属性值
        :return: 定位到的元素
        """
        try:
            return self.driver.switch_to_frame(loc)
        except Exception as msg:
            log.error("查找iframe异常-> {0}".format(msg))

    def switch_alert(self):
        """
        警告框处理
        :return:
        """
        try:
            return self.driver.switch_to_alert()
        except Exception as msg:
            log.error("查找alert弹出框异常-> {0}".format(msg))