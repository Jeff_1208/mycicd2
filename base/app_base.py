from appium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
import time
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from base.base import Base
from selenium.webdriver.support import expected_conditions as EC
from appium.webdriver.common.touch_action import TouchAction

class AppBase(Base):

    def __init__(self, driver):
        self.driver = driver

    def setSwithToChecked(self,loc):
        r =self.getEleCheckStatus(loc)
        if not r:
            self.click_ele(loc)

    def setSwithToUnChecked(self,loc):
        r =self.getEleCheckStatus(loc)
        if r:
            self.click_ele(loc)
            
    def getEleCheckStatus(self, loc):
        ele = self.find_ele(loc)
        status = ele.get_attribute("checked");
        if status.upper() == "TRUE":
            return True
        else:
            return False

    #以accessibility_id进行定位，对Android而言，就是content-description属性
    # driver.find_element_by_accessibility_id('push_button').click()
    def find_app_ele(self,loc):
        if loc[0]==By.ID:
            ele = self.driver.find_element_by_accessibility_id(loc[1])
        return ele

    def click_app_ele(self):
        self.find_app_ele().click()

    def input_app_ele(self,loc,text):
        ele = self.find_app_ele(loc)
        ele.clear()
        ele.send_keys(text)

    def wait_util_element_is_visible(self,loc, timeout=30, poll=0.5):
        WebDriverWait(self.driver,timeout,poll).until(EC.presence_of_element_located(loc))

    # 获取app包名和启动名#   获取包名方法：current_package#   获取启动名：current_activity
    def get_app_package_and_activity(self):
        return self.driver.current_package, self.driver.current_activity

    def launch_app(self):
        self.driver.launch_app()

    def close_app(self):
        self.driver.close_app()

    def reset_app(self):  #清除设置以后启动或重启 App
        self.driver.reset()

    def start_activity(self,activity_name):  #自己启动 activity,在原有 activity 上继续操作
        self.driver.start_activity(activity_name)

    def get_app_all_contexts(self):
        self.driver.contexts

    def get_app_context(self):
        self.driver.context
        # self.driver.current_context #效果与self.driver.context一致

    def switch_app_context(self,context_name):
        self.driver.switch_to.context(context_name)

    # 获取元素在屏幕上的坐标
    def get_element_location(self,loc):
        return self.find_ele(loc).location#   执行结果:{'y': 44, 'x': 408}

    def get_ele_text(self,loc):
        self.find_ele(loc).get_attribute("text")
        self.find_ele(loc).text

    #获取元素的属性值  方法: get_attribute(value) # value:元素的属性
    def get_ele_attrubute(self,loc,value):
        ele = self.find_ele(loc)
        if value =="name":  #value='name' 返回content-desc / text属性值
            return ele.get_attribute("name")
        elif value =="text":  #value='text' 返回text的属性值
            return ele.get_attribute("text")
        elif value =="className":  #value='className' 返回 class属性值
            return ele.get_attribute("className")
        elif value =="resourceId":  #value='resourceId' 返回 resource-id属性值
            return ele.get_attribute("resourceId")
        else:
            return ele.get_attribute("text")

    def scroll_up(self):
        # 获取屏幕宽高：
        width = self.driver.get_window_size()['width']
        height = self.driver.get_window_size()['height']
        self.driver.swipe(width/2,height*0.8,width/2,height*0.2)#滑动屏幕
        # self.driver.swipe(188,659,188,200)
        # TouchAction(self.driver).press(x=240,y=1000).move_to(x=0,y=-400).release().perform()

    def scrollup_until_element_clickable(self,loc,maxTime=30):
        ele = None
        count = 1
        while (True):
            if ele!=None or count==maxTime:
                break
            try:
                ele = self.find_ele_for_exception(loc,2)
            except:
                self.scroll_up()
                count+=1

    def swipeUp(self,driver,xpath):
        # 获取屏幕宽高：
        width = driver.get_window_size()['width']
        height = driver.get_window_size()['height']
        # while循环10次
        i=0
        while i < 10:
            try:
                driver.find_element_by_xpath(xpath).click()#尝试点击元素
                break
            except Exception as e:
                driver.swipe(width/2,height*0.8,width/2,height*0.2)#滑动屏幕
                i=i+1

    def wait_activity0(self, activity_name, timeout=5, interval=1):
        if self.driver.wait_activity(activity_name,timeout,interval):
            return True
        return False

    def is_right_activity(self,activity_name):
        if self.driver.current_activity() == activity_name:
            return True
        else:
            return False

    def wait_activity(self, activity, timeout, interval=1):
        try:
            WebDriverWait(self.driver, timeout, interval).until(lambda d: d.current_activity == activity)
            return True
        except Exception:
            return False

    #   swip滑动事件   从一个坐标位置滑动到另一个坐标位置,只能是两个点之间的滑动
    #   方法：swipe(start_x, start_y, end_x, end_y, duration=None)
    #   参数：
    #       1.start_x：起点X轴坐标
    #       2.start_y：起点Y轴坐标
    #       3.end_x：  终点X轴坐标
    #       4.end_y,： 终点Y轴坐标
    def app_swipe(self):
        # 从坐标(148,659)滑动到坐标(148,248)
        self.driver.swipe(188,659,148,248)# 滑动没有持续时间
        self.driver.swipe(188,659,148,248,5000)# 滑动持续5秒的时间

    # scroll滑动事件   从一个元素滑动到另一个元素，直到页面自动停止    方法：scroll(origin_el, destination_el)
    def app_ele_scroll(self,loc1,loc2):
        ele1 = self.find_ele(loc1)
        ele2 = self.find_ele(loc2)
        self.driver.scroll(ele1,ele2)  # 执行滑动操作

  # APP放置后台，模拟热启动       方法：background_app(seconds)
    def set_background_app(self,time=5):
        self.driver.background_app(time)  # app置于后台5s后，再次展示当前页面

    # 获取屏幕大小
    def getSize(self,driver):
        x = driver.get_window_size()['width']
        y = driver.get_window_size()['height']
        print('屏幕尺寸为：', driver.get_window_size())
        return (x, y)

    #    手指轻敲操作 模拟手指轻敲一下屏幕操作
    #   方法：tap(element=None, x=None, y=None)
    #   方法：perform() # 发送命令到服务器执行操作
    #   参数：
    #       1.element：被定位到的元素
    #       2.x：相对于元素左上角的坐标，通常会使用元素的X轴坐标
    #       3.y：通常会使用元素的Y轴坐标
    def app_ele_tap(self,loc):# 通过元素定位方式敲击屏幕
        ele = self.find_ele(loc)
        TouchAction(self.driver).tap(ele).perform()  #perform() # 发送命令到服务器执行操作

    def app_ele_tap_by_location(self,x,y):# 通过坐标方式敲击屏幕
        TouchAction(self.driver).tap(x=x,y=y).perform()

    # 手指按操作模拟手指按下屏幕,按就要对应着离开.
    # 方法:press(el=None, x=None, y=None)
    # 方法：release() # 结束动作，手指离开屏幕
    # 参数：
    #     1.element：被定位到的元素
    #     2.x：通常会使用元素的X轴坐标
    #     3.y：通常会使用元素的Y轴坐标
    def app_press_by_element(self,loc):# 通过元素定位方式按下屏幕
        ele = self.find_ele(loc)
        TouchAction(self.driver).press(ele).release().perform()

    # 通过坐标方式按下屏幕，WLAN坐标:x=155,y=250
    # TouchAction(driver).press(x=155,y=250).release().perform()
    def app_press_by_location(self,x,y):# 通过坐标方式按下屏幕
        TouchAction(self.driver).press(x=x,y=y).release().perform()

    # 等待操作   方法：wait(ms=0)  参数：ms：暂停的毫秒数
    def wait_press(self,loc):
        ele = self.find_ele(loc)
        TouchAction(self.driver).press(ele).wait(5000).perform()


    # 手指长按操作  模拟手机按下屏幕一段时间,按就要对应着离开.
    # 方法：long_press(el=None, x=None, y=None, duration=1000)
    # 参数：
    # 1.element：被定位到的元素
    # 2.x：通常会使用元素的X轴坐标
    # 3.y：通常会使用元素的Y轴坐标
    # 4.duration：持续时间，默认为1000ms
    # # 通过元素定位方式长按元素
    # TouchAction(driver).long_press(el,duration=5000).release().perform()
    def app_long_press_by_element(self,loc):# 通过元素定位方式长按元素
        ele = self.find_ele(loc)
        TouchAction(self.driver).long_press(ele).release().perform()

    # 通过坐标方式长按元素，WiredSSID坐标:x=770,y=667
    # 添加等待(有长按)／不添加等待(无长按效果)
    # TouchAction(driver).long_press(x=770,y=667).perform()
    def app_long_press_by_location(self,x,y):# 通过坐标方式长按元素
        TouchAction(self.driver).long_press(x=x,y=y).release().perform()

    # 模拟手机的滑动操作    方法：move_to(el=None, x=None, y=None)
    # 参数:
    # 1.el:定位的元素
    # 2.x:相对于前一个元素的X轴偏移量
    # 3.y:相对于前一个元素的Y轴偏移量
    # TouchAction(driver).press(el).move_to(el1).release().perform()
    def move_with_element_to_element(self,loc1,loc2):
        ele1 = self.find_ele(loc1)
        ele2 = self.find_ele(loc2)
        TouchAction(self.driver).press(ele1).move_to(ele2).release().perform()

    # 坐标的方式向上滑动
    # TouchAction(driver).press(x=240,y=1000).move_to(x=0,y=-400).release().perform()
    # press().move_to() 实际使用的是TouchAction方法，需要给相对坐标.
    def  move_by_location(self,x1,y1,x2,y2):
        TouchAction(self.driver).press(x=x1,y=y1).move_to(x=x2,y=y2).release().perform()

    # TouchAction(driver).press(x=240,y=600).wait(100).move_to(x=240,y=100).release().perform()
    #press().wait().move_to()实际调用的是swip方法，会向下拉，感觉属于bug，可在log中查询swip，不建议这么用.
    def  move_by_location_with_wait(self,x1,y1,x2,y2,waittime):
        TouchAction(self.driver).press(x=x1,y=y1).wait(waittime).move_to(x=x2,y=y2).release().perform()

    # drag拖拽事件
    #  从一个元素滑动到另一个元素,第二个元素替代第一个元素原本屏幕上的位置
    #   方法：drag_and_drop(origin_el, destination_el)
    def ele_drag_and_drop(self,loc1,loc2):
        ele1 = self.find_ele(loc1)
        ele2 = self.find_ele(loc2)
        self.driver.drag_and_drop(ele1,ele2)  # 执行滑动操作

    def move_with_more_location(self):
        # 绘制图案四个坐标 1:(244,967) 2:(723,967) 3:(723,1442) 4:(244,1916)
        #手机图形解锁操作
        TouchAction(self.driver).press(x=244,y=967).wait(100).move_to(x=479,y=0).wait(100)\
            .move_to(x=0,y=475).wait(100).move_to(x=-479,y=474).release().perform()

    # 获取当前手机的时间 方法：device_time 返回结果：Wed Dec 27 08:52:45 EST 2017
    def get_device_time(self):
        return self.driver.device_time

    #  获取手机的宽高，可以根据宽高做一些坐标的操作
    def get_windows_size(self):
        self.driver.get_window_size()  #    # print(driver.device_time)

    #   模拟系统键值的操作，比如操作home键，音量键,返回键等。 方法：keyevent(keycode, metastate=None):
    #   参数：keycode：发送给设备的关键代码
    #       metastate：关于被发送的关键代码的元信息，一般为默认值
    def click_keyboard(self,key):
        self.driver.keyevent(keycode=key)

    #   打开手机的通知栏，可以获取通知栏的相关信息和元素操作
    def get_notification(self):
        return self.driver.open_notifications()

    #   获取手机当前连接的网络
    def get_network_connection(self):
        return self.driver.network_connection

    # 更改手机的网络模式，模拟特殊网络情况下的测试用例
    # 方法：set_network_connection(connectionType)
    # 参数： connectionType：需要被设置成为的网络类型
    def set_network_connection(self,value):
        self.driver.set_network_connection(value)

    #   截取手机当前屏幕，保存指定格式图片到设定位置
    #   方法：get_screenshot_as_file(filename)
    #   参数： filename：指定路径下，指定格式的图片.
    def get_screenshot(self,filename):
        self.driver.get_screenshot_as_file(filename)

    # 向左滑，取Ｙ轴的中间值，即０.５（也可随意调整），改变Ｘ的位置，从右到左，即值从大到小，
    # 所以开始位置的坐标　乘以０.９，结束位置的坐标乘以０.１，即可实现
    def swipeLeft(self,driver,t):
        l = self.getSize()
        sX = int(l[0] * 0.9)
        sY = int(l[1] * 0.5)
        eX = int(l[0] * 0.1)
        driver.swipe(sX, sY, eX, sY, t)

    # 向下滑
    def swipeDown(self,driver,t):
        l = self.getSize()
        sX = int(l[0] * 0.5)
        sY = int(l[1] * 0.3)
        eY = int(l[1] * 0.9)
        driver.swipe(sX, sY, sX, eY, t)

    #向右滑
    def swipeRight(self,driver,t):
        l = self.getSize()
        sX = int(l[0] * 0.1)
        sY = int(l[1] * 0.5)
        eX = int(l[0] * 0.9)
        driver.swipe(sX, sY, eX, sY, t)

    #向上滑
    def swipeUp(self,driver,t):
        l = self.getSize()
        sX = int(l[0] * 0.5)
        sY = int(l[1] * 0.9)
        eY = int(l[1] * 0.3)
        driver.swipe(sX, sY, sX, eY, t)

    # 调用滑动方法
    # swipeLeft(2000)
    # swipeRight(2000)
    # swipeDown(2000)
    # swipeUp(2000)

"""
Appium+Python 自动化-appium常用元素定位方法
ID定位
# resourceId属性的方法
driver.find_element_by_id('com.lizi.app:id/setting_imageView').click()
#以accessibility_id进行定位，对Android而言，就是content-description属性
driver.find_element_by_accessibility_id('push_button').click()

ClassName 定位

# 定位唯一元素
self.driver.find_element_by_class_name("android.widget.EditText")
# 找到所有android.widget.EditText并定位第一个
self.driver.find_elements_by_class_name("android.widget.EditText")[0]

Name 定位

#根据name进行定位，对于android来说，就是text属性
driver.find_element_by_name(u"登 录").click（）　

 Xpath 定位

driver.find_elements_by_xpath('//android.widget.TextView[@resource-id="com.mzdk.app:id/item_good_title"]')[1].click()


Uiautomator 定位

text属性的方法
driver.find_element_by_android_uiautomator('new UiSelector().text("Custom View")').click()         #text
driver.find_element_by_android_uiautomator('new UiSelector().textContains("View")').click()        #textContains
driver.find_element_by_android_uiautomator('new UiSelector().textStartsWith("Custom")').click()    #textStartsWith
driver.find_element_by_android_uiautomator('new UiSelector().textMatches("^Custom.*")').click()    #textMatches

class属性的方法

#className
driver.find_element_by_android_uiautomator('new UiSelector().className("android.widget.TextView").text("Custom View")').click()
#classNameMatches
driver.find_element_by_android_uiautomator('new UiSelector().classNameMatches(".*TextView$").text("Custom View")').click()

 resourceId属性的方法
#resourceId
driver.find_element_by_android_uiautomator('new UiSelector().resourceId("android:id/text1")')
#resourceIdMatches
driver.find_element_by_android_uiautomator('new UiSelector().resourceIdMatches(".*id/text1$")')

 元素的其他属性
driver.find_element_by_android_uiautomator('new UiSelector().clickable(true).text("Custom View")').click()

"""

