__author__ = 'Jeff.xie'
# encoding:utf-8
from selenium import webdriver
from appium import webdriver as appdriver
import time

def driver_init():
    return webdriver.Chrome()

def app_driver_init():
    desired_caps = {}
    # 系统
    desired_caps['platformName'] = 'Android'
    #手机版本，在手机中：设置--关于手机
    desired_caps['platformVersion'] = '8.0.0'
    # 设备号
    desired_caps['deviceName'] = 'd494a011'
    # 包名
    # desired_caps['appPackage'] = 'welab.bank.mobile.stage'
    desired_caps['appPackage'] = 'com.android.settings'
    # 启动名
    desired_caps['appActivity'] = '.Settings'
    desired_caps["unicodeKeyboard"] = "True"#appium提供的一种输入法，可以传中文。测试时直接用这个输入法
    desired_caps["resetKeyboard"] = "True"#程序结束时重置原来的输入法
    desired_caps["noReset"] = "True"#不初始化手机app信息（类似不清除缓存）
    desired_caps["autoLaunch"] = "False"#有时不想让 appium 每次都启动 app，而是想自己打开 activity 或者在原来 activity 基础上继续操作，可以使用此项
    # 声明手机驱动对象   #192.168.56.1
    print(time.strftime("%H:%M:%S"))
    driver = appdriver.Remote("http://127.0.0.1:4723/wd/hub",desired_caps)  #/wd/hub固定写法
    driver.wait_activity()
    return driver




# 查看app 的 package和activity 需要开启 app，然后执行下面的命令
# adb shell dumpsys window w | findstr \/ | findstr name=
def android_avd_app_driver_init():
    desired_caps = {}
    # 系统
    desired_caps['platformName'] = 'Android'
    #手机版本，在手机中：设置--关于手机
    desired_caps['platformVersion'] = '10.0.0'
    # 设备号
    desired_caps['deviceName'] = 'emulator-5554'
    # 包名
    desired_caps['appPackage'] = 'welab.bank.mobile.stage'
    # 启动名
    desired_caps['appActivity'] = 'com.welabfrontend.MainActivity'
    desired_caps["unicodeKeyboard"] = "True"#appium提供的一种输入法，可以传中文。测试时直接用这个输入法
    desired_caps["resetKeyboard"] = "True"#程序结束时重置原来的输入法
    desired_caps["noReset"] = "True"#不初始化手机app信息（类似不清除缓存）
    desired_caps["autoLaunch"] = "False"#有时不想让 appium 每次都启动 app，而是想自己打开 activity 或者在原来 activity 基础上继续操作，可以使用此项
    # 声明手机驱动对象   #192.168.56.1
    # print(time.strftime("%H:%M:%S"))
    driver = appdriver.Remote("http://127.0.0.1:4723/wd/hub",desired_caps)  #/wd/hub固定写法
    time.sleep(20)
    return driver




