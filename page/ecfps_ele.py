__author__ = 'Jeff.xie'
from selenium.webdriver.common.by import By


class ECPFS_Ele():

    gaoji_btn = (By.XPATH, '//*[@id="details-button"]') # 点击高级
    going_link = (By.XPATH, '//*[@id="proceed-link"]') # 点击进入链接
    #输入用户名密码,登录提交按钮
    login_username = (By.XPATH, '//*[@id="login-form"]/div[1]/input')# 输入 用户名
    login_pw = (By.XPATH, '//*[@id="login-form"]/div[2]/input')# 输入 密码
    login_submmit_btn = (By.XPATH, '//*[@id="login-form"]/div[3]/button')

    payment_menu= (By.XPATH, '//*[@id="side-menu"]/li[4]/a') # 点击Payment Service
    credit_transfer= (By.XPATH, '//*[@id="side-menu"]/li[4]/ul/li[1]/a') # 点击 Credit Transfer
    payment_enquiry= (By.XPATH, '//*[@id="side-menu"]/li[4]/ul/li[3]/a') # 点击 Payment Enquiry

    payment_excepption_menu= (By.XPATH, '//*[@id="side-menu"]/li[4]/ul/li[4]/a') # 点击Payment_Excepption
    date_input=(By.XPATH,'//*[@id="startCreateDate"]')
    time_menu_1= (By.XPATH, '//*[@id="basicStartCreateDate"]/span[3]') # 选择时间3步骤
    time_menu_2= (By.XPATH, '/html/body/div[15]/div[3]/table/thead/tr[1]/th[1]')
    time_menu_3= (By.XPATH, '/html/body/div[15]/div[3]/table/tbody/tr[3]/td[1]')
    search_btn= (By.XPATH, '//*[@id="searchBtn"]') # 点击search

    next_page_btn= (By.XPATH, '//*[@id="page-wrapper"]/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[2]/ul/li[8]/a') # 点击next page
    view_detail_btn= (By.XPATH, '//*[@id="viewDetailBtn"]') # 点击View detail
    Creditor_Agent= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[4]/div[2]/div/div[2]/div[1]/div/input') # 点击
    ref_input= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[6]/div/div[1]/div[2]/div[2]/div[1]/div/div/input')
    close_fuhao= (By.XPATH, '//*[@id="viewPaymentDetailModal"]/div[2]/div/div[1]/button')
    close_btn= (By.XPATH, '//*[@id="viewPaymentDetailModal"]/div[2]/div/div[2]/button[7]')

    scroll_to_bottom= "window.scrollTo(0,document.documentElement.scrollHeight);"
    # scroll_to_bottom= "window.scrollTo(0,document.body.scrollHeight);"
    # scroll_to_bottom= "var q=document.documentElement.scrollTop=10000"
    scroll_to_top= "var q=document.documentElement.scrollTop=0"
    # js = 'var action=document.documentElement.scrollTop=10000'
    js = 'var action=document.body.scrollTop=10000'
    # scroll_to_bottom= 'window.scrollTo(0,500);'
    # scroll_to_bottom= 'window.scrollBy(0,5000);'

    status_btn= (By.XPATH, '//*[@id="enquireIclStatusBtn"]')


    payment_servie_type_select= (By.XPATH, '//*[@id="businessService"]')
    inquire_btn= (By.XPATH, '//*[@id="inquireDebtorAcctBtn"]')
    debtor_ac_input= (By.XPATH, '//*[@id="creditTransferForm"]/div[2]/div/div/div/div/input')
    debtor_name_input= (By.XPATH, '//*[@id="creditTransferForm"]/div[3]/div[1]/div/div/select')

    amount_input= (By.XPATH, '//*[@id="creditTransferForm"]/div[4]/div/div/div/div/input')
    payment_category_select= (By.XPATH, '//*[@id="creditTransferForm"]/div[6]/div/div/div/select')

    creditor_agent_select= (By.XPATH, '//*[@id="creditorAgentList"]')
    creditor_ac_input= (By.XPATH, '//*[@id="creditTransferForm"]/div[9]/div/div/div[2]/div[2]/div/div/div/div/input')
    creditor_name_input= (By.XPATH, '//*[@id="creditTransferForm"]/div[9]/div/div/div[2]/div[3]/div[1]/div/div/input')
    creditTransfer_confirm= (By.XPATH, '//*[@id="creditTransferForm"]/div[10]/div/button[1]')
    # bank_390_xpath= (By.XPATH, '//*[@id="creditorAgentList"]/option[167]')
    bank_390_xpath='//*[@id="creditorAgentList"]/option[167]'

    show_transfer_success_xpath=(By.XPATH,'/html/body/div[2]/div/div[2]/div')


    approvel_menu= (By.XPATH, '//*[@id="side-menu"]/li[5]/a')
    request_approvel= (By.XPATH, '//*[@id="side-menu"]/li[5]/ul/li/a')
    approved_selected= (By.XPATH, '//*[@id="page-wrapper"]/div[2]/div/div[1]/div/div')
    approved_selected_checkbox= (By.XPATH, '//*[@id="data-table"]/tbody/tr/td[1]/input')
    approved_confirm= (By.XPATH, '//*[@id="approveRequestsForm"]/div[2]/button[1]')


    logout_1= (By.XPATH, '//*[@id="navbarbar"]/ul/li/a')
    logout_2= (By.XPATH, '//*[@id="btnLogout"]')
    logout_ok= (By.XPATH, '/html/body/div[2]/div[2]/div/div[2]/button[2]')
    logout_ok_2= (By.XPATH, '/html/body/div[3]/div[2]/div/div[2]/button[2]')


    addressing_service_menu= (By.XPATH, '//*[@id="side-menu"]/li[6]/a')
    manage_proxy_records = (By.XPATH, '//*[@id="side-menu"]/li[6]/ul/li[1]/a')
    bank_customer_id= (By.XPATH, '//*[@id="srchCustId"]')
    fps_register_record1_user= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[2]')
    fps_register_type= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[5]')

    register_btn= (By.XPATH, '//*[@id="regBtn"]')
    proxy_id= (By.XPATH, '//*[@id="regProxyId"]')



    fps_ref_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[4]')
    fps_ref_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[4]')
    inward_outward_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[5]')
    inward_outward_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[5]')
    deptor_ac_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[7]')
    deptor_ac_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[7]')
    creditor_ac_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[8]')
    creditor_ac_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[8]')
    amount_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[10]')
    amount_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[10]')
    payment_status_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[12]')
    payment_status_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[12]')
    balance_status_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[13]')
    balance_status_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[13]')
    channel_check_outward= (By.XPATH, '//*[@id="data-table"]/tbody/tr[1]/td[14]')
    channel_check_inward   = (By.XPATH, '//*[@id="data-table"]/tbody/tr[2]/td[14]')

    # payment enquiry

    item_check_box_path= '//*[@id="data-table"]/tbody/tr[{}]/td[1]/input'
    fps_ref_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[4]'
    inward_outward_check= '//*[@id="data-table"]/tbody/tr[{}]/td[5]'
    deptor_ac_check= '//*[@id="data-table"]/tbody/tr[{}]/td[7]'
    creditor_ac_check= '//*[@id="data-table"]/tbody/tr[{}]/td[8]'
    amount_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[10]'
    status_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[11]'
    payment_status_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[12]'
    balance_status_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[13]'
    channel_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[14]'

    # payment exception
    exception_first_item_path= (By.XPATH,  '//*[@id="data-table"]/tbody/tr[1]/td[1]/input')
    exception_item_check_box_path= '//*[@id="data-table"]/tbody/tr[{}]/td[1]/input'
    exception_fps_ref_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[4]'
    exception_inward_outward_check= '//*[@id="data-table"]/tbody/tr[{}]/td[5]'
    exception_deptor_ac_check= '//*[@id="data-table"]/tbody/tr[{}]/td[7]'
    exception_creditor_ac_check= '//*[@id="data-table"]/tbody/tr[{}]/td[8]'
    exception_amount_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[10]'
    exception_status_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[12]'
    exception_balance_status_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[13]'
    exception_exception_status_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[14]'
    exception_exception_msg_check=  '//*[@id="data-table"]/tbody/tr[{}]/td[15]'

    '''
    FPS Ref
    //*[@id="data-table"]/tbody/tr[1]/td[4]
    Inward /Outward
    //*[@id="data-table"]/tbody/tr[1]/td[5]
    Deptor A/C
    //*[@id="data-table"]/tbody/tr[1]/td[7]/div
    //*[@id="data-table"]/tbody/tr[2]/td[7]/div

    Creditor A/C
    //*[@id="data-table"]/tbody/tr[1]/td[8]/div
    //*[@id="data-table"]/tbody/tr[2]/td[8]/div
    amount check
    //*[@id="data-table"]/tbody/tr[1]/td[10]
    Payment Status   Accepted & Settled
    //*[@id="data-table"]/tbody/tr[1]/td[12]/div

    Balance Status  POSTED
    //*[@id="data-table"]/tbody/tr[1]/td[13]/div

    channel BOA/ECFPS
    outward BOA  inward  ECFPS
    //*[@id="data-table"]/tbody/tr[1]/td[14]
    '''


    # reg_18


    payment_exception_status_search_btn= (By.XPATH, '//*[@id="simpleSearchForm"]/div[3]/div[2]/div/div[2]/button')
    payment_exception_status_select_all= (By.XPATH, '//*[@id="simpleSearchForm"]/div[3]/div[2]/div/div[2]/ul/li[1]/a/label/input')


    payment_excepption_ref_input = (By.XPATH, '//*[@id="dropdownTextBox"]')

    items_total_page_xpath= (By.XPATH, '//*[@id="page-wrapper"]/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[1]/span[1]')
    Ledger_tx_status_detail_btn= (By.XPATH, '//*[@id="balanceTxDetailBtn"]')
    Ledger_tx_status_ref= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[6]/div/div[1]/div[2]/div[3]/div[2]/div/div/input')

    reprocess_redger_transaction_btn= (By.XPATH, '//*[@id="reprocessLedgerTxBtn"]')
    redger_transaction_details_status= (By.XPATH, '//*[@id="ledgerTxDetailTable"]/tbody/tr/td[7]/div')
    redger_transaction_details_type= (By.XPATH, '//*[@id="ledgerTxDetailTable"]/tbody/tr/td[4]')
    redger_transaction_details_amount= (By.XPATH, '//*[@id="ledgerTxDetailTable"]/tbody/tr/td[5]')
    redger_transaction_details_ref= (By.XPATH, '//*[@id="ledgerTxDetailTable"]/tbody/tr/td[6]')
    redger_transaction_close_btn= (By.XPATH, '//*[@id="ledgerTxDetailModal"]/div[2]/div/div/button')

    redger_transaction_close_single= (By.XPATH, '//*[@id="viewPaymentDetailModal"]/div[2]/div/div[1]/button')
    ledger_tx_status_value= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[6]/div/div[1]/div[2]/div[3]/div[1]/div/div/div/input')
    ledger_tx_ref_value= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[6]/div/div[1]/div[2]/div[3]/div[2]/div/div/input')
    payment_detail_close_btn= (By.XPATH, '//*[@id="viewPaymentDetailModal"]/div[2]/div/div[2]/button[7]')
    payment_detail_close_single= (By.XPATH, '//*[@id="viewPaymentDetailModal"]/div[2]/div/div[1]/button')

    payment_result_in_payment_detail= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[6]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/input')
    exception_status_in_payment_detail= (By.XPATH, '//*[@id="paymentDetailForm"]/div/div[6]/div/div[1]/div[2]/div[4]/div[2]/div/div/div/input')



    view_journal_btn= (By.XPATH, '//*[@id="viewHistoryBtn"]')
    request_event_log_status= (By.XPATH, '//*[@id="eventTable"]/tbody/tr[1]/td[3]')

    enquireIclStatusBtn= (By.XPATH, '//*[@id="enquireIclStatusBtn"]')



    fps_ref_no_transaction_drop_btn= (By.XPATH, '//*[@id="searchList"]/button/span[2]')
    fps_ref_no_item= (By.XPATH, '//*[@id="searchList"]/ul/li[1]/a')
    transaction_id_item= (By.XPATH, '//*[@id="searchList"]/ul/li[2]/a')
    # creditor_agent_select= (By.XPATH, '')
    # creditor_agent_select= (By.XPATH, '')
    # creditor_agent_select= (By.XPATH, '')
    # creditor_agent_select= (By.XPATH, '')
    # creditor_agent_select= (By.XPATH, '')





