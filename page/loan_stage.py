__author__ = 'Jeff.xie'

from base.base import Base
from page.loan_ele_stage import Loan_ele_Stage
from selenium.webdriver.common.by import By

class Loan(Base):

    def __init__(self,driver):
        Base.__init__(self,driver)
        self.loan_amount =0
        self.first_window = ""
        self.status=""

    def open_url(self,url="https://ploan-portal.sta-wlab.net/"):
        self.driver.get(url)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()
        self.first_window =self.driver.current_window_handle

    def click_login_btn(self):
        self.click_ele(Loan_ele_Stage.login_btn)

    def click_gaoji(self):
        self.click_ele(Loan_ele_Stage.gaoji_btn)
        self.click_ele(Loan_ele_Stage.going_link)

    def login_stage(self,username,pw):
        self.input_ele(Loan_ele_Stage.login_username,username)
        self.input_ele(Loan_ele_Stage.login_pw,pw)
        self.click_ele(Loan_ele_Stage.login_submmit_btn)

    def logout(self):
        self.click_ele(Loan_ele_Stage.account_xpath)
        self.click_ele(Loan_ele_Stage.logout_xpath)

    def click_application(self):
        self.click_ele(Loan_ele_Stage.application_xpath)

    def search_application(self,ID):
        self.input_ele(Loan_ele_Stage.loan_id_input,ID)
        self.click_ele(Loan_ele_Stage.search_btn)

    def assign_owner(self, name):
        self.select_a_ele_from_select(Loan_ele_Stage.assign_select_xpath,name)

    def click_detail(self):
        self.click_ele(Loan_ele_Stage.loan_detail)

    def get_loan_amout(self):
        self.switch_window()
        try:
            ele = self.find_ele(Loan_ele_Stage.loan_amount_1)
        except Exception as e:
            ele = self.find_ele(Loan_ele_Stage.loan_amount_2)
        self.loan_amount =self.amount_to_int(ele.text)
        self.close_and_switch_window(self.first_window)
        return self.loan_amount

    def amount_to_int(self,amount):
        amount= amount.replace("$","")
        amount= amount.replace(",","")
        return int(amount)

    # def close_and_switch_window(self,first_window):
    #     self.close_and_switch_window(first_window)

    def switch_window(self):
        self.switch_windows()

    def first_approved(self,value,ID):
        self.search_application(ID)
        self.click_detail()
        self.switch_window()
        self.click_ele(Loan_ele_Stage.Residential_Info_xpath)
        self.scroll_windows(Loan_ele_Stage.first_approved_scroll_js_1)
        self.click_ele(Loan_ele_Stage.Work_Info_xpath)
        self.scroll_windows(Loan_ele_Stage.first_approved_scroll_js_2)
        self.click_ele(Loan_ele_Stage.Public_data_xpath)
        self.scroll_windows(Loan_ele_Stage.first_approved_scroll_js_3)
        self.click_first_income_proof_valid()
        self.scroll_windows_top()
        self.click_quick_edit()
        self.select_a_ele_from_select(Loan_ele_Stage.status_xpath,value)
        self.select_reason()

    def second_approved(self, value,ID):
        self.click_application()
        self.search_application(ID)
        self.click_detail()
        self.switch_window()
        self.click_second_income_proof_valid()
        self.scroll_windows_top()
        self.click_quick_edit()
        self.select_a_ele_from_select(Loan_ele_Stage.status_xpath,value)
        self.click_update()
        self.close_and_switch_window(self.first_window)

    def third_approved(self):
        self.click_appliation()
        self.search_application()
        self.click_detail()
        self.switch_window()
        self.click_credit_info()
        self.click_approved()

    def check_loans_result(self,user,pw,ID):
        self.logout()
        self.login_stage(user,pw)
        self.search_application()
        self.status = self.get_loans_status(ID)
        if self.status  == "Approved":
            print(self.loan_application_number, " is Approved!!!!!")
        else:
            print(self.loan_application_number, " is ",self.status ," !!!!!")
            raise  Exception
        return  self.status

    def first_income_proof_generate_js(self,start,end,add,index):
        start_1 = start + (index - 1) * add
        end_1 = end + (index- 1) * add
        return 'window.scrollTo({},{});'.format(str(start_1),str(end_1+1))

    def scroll_windows_top(self):
        self.scroll_windows(Loan_ele_Stage.windows_top)

    def click_quick_edit(self):
        self.click_ele(Loan_ele_Stage.Quick_Edit_xpath)

    def select_reason(self):
        self.click_ele(Loan_ele_Stage.reason_select)
        self.click_ele(Loan_ele_Stage.reason_item)

    def select_status(self,value):
        self.select_a_ele_from_select(Loan_ele_Stage.status_xpath,value)

    def select_status_by_flag(self,flag,value):
        self.select_a_ele_from_select_by_flag(Loan_ele_Stage.status_xpath,flag,value)

    def get_loans_status(self,ID):
        self.click_application()
        self.search_application(ID)
        print(ID," status: ",self.find_ele(Loan_ele_Stage.status_xpath).text,"!!!!!!!!!!!!!!!")
        return self.find_ele(Loan_ele_Stage.status_xpath).text

    def click_credit_info(self):
        self.click_ele(Loan_ele_Stage.Credit_Info_xpath)#点击Credit Info

    def click_approved(self):
        self.click_ele(Loan_ele_Stage.Approved_xpath)#点击approved

    def click_first_income_proof_valid(self,):
        try:
            for i in range(1, 100):# 地址证明
                income_pass_xpath = '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[7]/div[2]/div/div[1]/div[{}]/ul/div/li[1]/div[1]/div[1]/label/input'.format(str(i))
                self.click_ele((By.XPATH,income_pass_xpath))
                self.scroll_windows(self.first_income_proof_generate_js(1200,1400,370,i))
        except Exception as e:
            print("第一次审批点击第" + str(i) + "个Address Proof Failed!!!")
        self.scroll_windows(Loan_ele_Stage.address_income_js_1)
        try:
            for i in range(1, 100):# 收入证明
                income_pass_xpath = '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[7]/div[2]/div/div[2]/div[{}]/ul/div/li[1]/div[1]/div[1]/label/input'.format(str(i))
                self.click_ele((By.XPATH,income_pass_xpath))
                self.scroll_windows(self.first_income_proof_generate_js(1200,1400,370,i))
        except Exception as e:
            print("第一次审批点击第" + str(i) + "个Income Proof Failed!!!")

    def click_second_income_proof_valid(self,):
        try:
            for i in range(1, 100):# 地址证明
                income_pass_xpath = '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[7]/div[2]/div/div[1]/div[{}]/ul/div/li[2]/div[1]/div[1]/label/input'.format(str(i))
                self.click_ele((By.XPATH,income_pass_xpath))
                self.scroll_windows(self.first_income_proof_generate_js(1400,1700,350,i))
        except Exception as e:
            print("第二次审批点击第" + str(i) + "个Address Proof Failed!!!")
        self.scroll_windows(Loan_ele_Stage.address_income_js_2)
        try:
            for i in range(1, 100):# 收入证明
                income_pass_xpath = '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[7]/div[2]/div/div[2]/div[{}]/ul/div/li[2]/div[1]/div[1]/label/input'.format(str(i))
                self.click_ele((By.XPATH,income_pass_xpath))
                self.scroll_windows(self.first_income_proof_generate_js(1400,1700,350,i))
        except Exception as e:
            print("第二次审批点击第" + str(i) + "个Income Proof Failed!!!")
