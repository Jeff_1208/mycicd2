__author__ = 'Jeff.xie'

from selenium.webdriver.common.by import By

class Loan_ele_Stage:
    gaoji_btn = (By.XPATH, '//*[@id="details-button"]') # 点击高级
    going_link = (By.XPATH, '//*[@id="proceed-link"]') # 点击进入链接
    #输入用户名密码,登录提交按钮
    login_btn = (By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li')# 点击Login
    login_username = (By.XPATH, '//*[@id="username"]')# 输入 用户名
    login_pw = (By.XPATH,  '//*[@id="password"]')# 输入 密码
    login_submmit_btn = (By.XPATH, '//*[@id="kc-login"]')# 点击登录

    loan_amount_1=(By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div[4]/div/span')
    loan_amount_2=(By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[2]/div[2]/div/div[1]/div[4]/div/span')
    assign_select_xpath = (By.XPATH,'//*[@id="root"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[11]/select')# 点击assign下来菜单，选择assign name

    account_xpath= (By.XPATH, '//*[@id="navbarDropdown"]')# 点击账户
    logout_xpath = (By.XPATH,  '//*[@id="navbarSupportedContent"]/ul/li/div/button')# 点击Log Out

    loan_id_input = (By.XPATH, '//*[@id="search"]')# 输入贷款number
    search_btn = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[1]/div/form/button')# 点击search

    loan_detail = (By.CLASS_NAME, 'detail')
    first_approved_scroll_js_1 = "window.scrollTo(0,300);"
    first_approved_scroll_js_2 = "window.scrollTo(300,600);"
    first_approved_scroll_js_3 = "window.scrollTo(600,900);"

    Residential_Info_xpath = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[4]/div[1]/fieldset/div[1]/label/input')# 点击Residential Info pass
    Work_Info_xpath = (By.XPATH,  '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[5]/div[1]/fieldset/div[1]/label/input')# 点击Work Info pass
    Public_data_xpath = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[6]/div[1]/fieldset/div[1]/label/input')# 点击Public data pass

    Quick_Edit_xpath = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/div/a/div[2]')# 点击Quick Edit
    # reason_xpath = (By.XPATH,  '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[2]/div[2]/form/div/div[3]/div/div/div/div[1]/div[1]/div[2]/svg/path')# 选择Reason
    # 选择Reason选择，这个不是下拉框，手动找不到xpath，需要print page_source来定位 # print(driver.page_source)
    reason_select = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[2]/div[2]/form/div/div[3]/div/div/div/div[1]/div[1]' ) # 点击显示下拉内容
    reason_item = (By.ID, 'react-select-2-option-0')
    huankuan_xpath = (By.XPATH,  '//*[@id="root"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/form/div/div[8]/div/input')# 输入还款信息
    update_xpath= (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[2]/div[2]/form/div/div[9]/div[2]/button')# 点击Update

    application_xpath= (By.XPATH, '//*[@id="sidebar-container"]/ul/li[1]/a')#點擊左上角Application
    windows_top = "var q=document.documentElement.scrollTop=0"# 将滚动条移动到页面的顶部
    address_income_js_1 = "var q=document.documentElement.scrollTop=1200"
    address_income_js_2 = "var q=document.documentElement.scrollTop=1400"

    Credit_Info_xpath= (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[2]/ul/li[3]/a')#点击Credit Info
    Approved_xpath = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div[3]/div/div[1]/div[1]/div[2]/div[2]/button')#点击Approved按钮
    pengding_checker_status_value='makerCompleted'
    check_complete_status_value='checkerCompleted'
    status_xpath = (By.XPATH, '//*[@id="root"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[9]/span')# check last status