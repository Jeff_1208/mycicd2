__author__ = 'Jeff.xie'

from base.base import Base
from selenium.webdriver.common.by import By
from page.ecfps_ele import ECPFS_Ele

class ECFPS(Base):

    def __init__(self,driver):
        Base.__init__(self,driver)

    def open_url(self,url="https://ecfps.sta-wlab.net/boa/showMain.action"):
        self.driver.get(url)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()

    def login(self,username,pw):
        self.click_ele(ECPFS_Ele.gaoji_btn)
        self.click_ele(ECPFS_Ele.going_link)
        self.input_ele(ECPFS_Ele.login_username,username)
        self.input_ele(ECPFS_Ele.login_pw,pw)
        self.click_ele(ECPFS_Ele.login_submmit_btn)


    def search_item(self,date):
        self.click_ele(ECPFS_Ele.payment_menu)# 点击Payment
        self.click_ele(ECPFS_Ele.payment_excepption_menu)# 点击Payment_Excepption
        ele=self.find_ele(ECPFS_Ele.date_input)
        js = 'arguments[0].removeAttribute("readonly");'
        self.execute_js(js,ele)
        self.input_ele(ECPFS_Ele.date_input,date)    #下面这行代码也有效
        # ele.send_keys(date)  # 时间为 年-月-日 "2021-5-21"
        '''
        # self.click_ele(ECPFS_Ele.time_menu_1) # 选择时间3步骤
        # self.click_ele(ECPFS_Ele.time_menu_2)
        # self.click_ele(ECPFS_Ele.time_menu_3)
        '''
        self.click_ele(ECPFS_Ele.search_btn)# 点击search

    def approved(self,attribute_value):
        for i in range(1,11):
            check_box_path = '//*[@id="data-table"]/tbody/tr['+str(i)+']/td[1]/input'
            print(check_box_path)
            self.click_ele((By.XPATH,check_box_path))
            self.click_ele(ECPFS_Ele.view_detail_btn)
            agent_text =self.get_ele_attribute_value(ECPFS_Ele.Creditor_Agent,attribute_value)
            if '672'  in agent_text or "390"  in agent_text:
                print(" Creditor Agent  is %s" %agent_text," didn't need click")
                self.click_ele(ECPFS_Ele.close_fuhao)
            else:
                self.scroll_windows(ECPFS_Ele.scroll_to_bottom)
                try:
                    self.click_ele(ECPFS_Ele.status_btn)
                except:
                    pass
                self.click_ele(ECPFS_Ele.close_btn)
                print(" Creditor Agent  is %s" %agent_text, " need to click")
            self.scroll_windows(ECPFS_Ele.scroll_to_bottom)

    # def click_all(self,attribute_value):
    #     for i in range(100):
    #         self.click_ele(ECPFS_Ele.next_page_btn)
    #         self.click_each_Excepption(attribute_value)



