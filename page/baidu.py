__author__ = 'Jeff.xie'
from base.base import Base
from page.element_location import Locations
from time import  sleep

class Baidu(Base):

    def __init__(self,driver):
        Base.__init__(self,driver)

    def open_url(self,url="https://www.baidu.com"):
        self.driver.get(url)

    def search_item(self,item):
        self.input_ele(Locations.baidu_input,item)
        self.click_ele(Locations.baidu_btn)
        sleep(2)
        first_window = self.driver.current_window_handle
        windows = self.driver.window_handles
        for window in windows:
            if window not in first_window:
                self.driver.switch_to.window(window)
                self.driver.close()
                break
        self.driver.switch_to.window(first_window)






