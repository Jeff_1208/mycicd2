__author__ = 'Jeff.xie'


from base.base import Base
from selenium.webdriver.common.by import By
from page.ecfps_ele import ECPFS_Ele
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep


class ECFPS_Reg(Base):

    def __init__(self,driver):
        Base.__init__(self,driver)

    def open_url(self,url="https://ecfps.sta-wlab.net/boa/showMain.action"):
        self.driver.get(url)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()

    def logout(self):
        self.click_ele(ECPFS_Ele.logout_1)
        self.click_ele(ECPFS_Ele.logout_2)
        try:
            self.click_ele(ECPFS_Ele.logout_ok_2)
        except:
            self.click_ele(ECPFS_Ele.logout_ok)

    def login(self,username,pw):
        self.input_ele(ECPFS_Ele.login_username,username)
        self.input_ele(ECPFS_Ele.login_pw,pw)
        self.click_ele(ECPFS_Ele.login_submmit_btn)

    def login_first(self,username,pw):
        self.click_ele(ECPFS_Ele.gaoji_btn)
        self.click_ele(ECPFS_Ele.going_link)
        self.login(username,pw)

    def get_payment_exception(self):
        self.click_payment_service()
        self.click_payment_excepption()

    def click_payment_service(self):
        self.click_ele(ECPFS_Ele.payment_menu)# 点击Payment Service

    def click_payment_excepption(self):
        self.click_ele(ECPFS_Ele.payment_excepption_menu)# 点击Payment_Excepption

    def click_credit_transfer(self):
        self.click_ele(ECPFS_Ele.credit_transfer)

    def input_credit_transfer(self,debtor_ac,amount,category_value,):
        self.click_payment_service()
        self.click_credit_transfer()
        sleep(2)
        self.scroll_windows_to_bottom()
        sleep(2)
        self.select_a_ele_from_select(ECPFS_Ele.payment_servie_type_select,"C2 Credit Transfer (PAYC02)")
        self.input_ele(ECPFS_Ele.debtor_ac_input,debtor_ac)
        self.click_ele(ECPFS_Ele.inquire_btn)
        self.input_ele(ECPFS_Ele.amount_input,amount)
        self.select_a_ele_from_select(ECPFS_Ele.payment_category_select,category_value)

    def input_creditor_transfer(self,creditor_ac,creditor_name,agent_value):
        self.select_a_ele_from_select_by_flag(ECPFS_Ele.creditor_agent_select,"value",agent_value,1)
        # self.click_ele(ECPFS_Ele.creditor_agent_select)
        # action=ActionChains(self.driver)
        # action.move_to_element(self.driver.find_element_by_xpath(ECPFS_Ele.bank_390_xpath)).send_keys(Keys.UP).perform()#下拉框下拉选项
        self.input_ele(ECPFS_Ele.creditor_ac_input,creditor_ac)
        self.input_ele(ECPFS_Ele.creditor_name_input,creditor_name)
        self.click_ele(ECPFS_Ele.creditTransfer_confirm)
        self.click_ele(ECPFS_Ele.show_transfer_success_xpath)

    def trans_approved(self):
        self.click_ele(ECPFS_Ele.approvel_menu)
        self.click_ele(ECPFS_Ele.request_approvel)
        self.click_ele(ECPFS_Ele.approved_selected_checkbox)
        self.click_ele(ECPFS_Ele.approved_selected)
        self.click_ele(ECPFS_Ele.approved_confirm)

    def check_register_fps(self):
        self.click_ele(ECPFS_Ele.amount_check)

    def click_payment_enquiry(self):
        self.click_payment_service()
        self.click_ele(ECPFS_Ele.payment_enquiry)



    def check_record_by_ac(self,debtor_ac,creditor_ac,amount):
        is_get_inward_trans= False
        is_get_outward_trans= False
        for i in range(1,11):
            if is_get_outward_trans and is_get_inward_trans:
                break
            deptor_ac_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.deptor_ac_check.format(str(i))))
            amount_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.amount_check.format(str(i))))
            creditor_ac_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.creditor_ac_check.format(str(i))))
            if debtor_ac in deptor_ac_check and amount in amount_check and creditor_ac in creditor_ac_check:
                fps_ref_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.fps_ref_check.format(str(i))))
                inward_outward_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.inward_outward_check.format(str(i))))
                amount_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.amount_check.format(str(i))))
                payment_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.payment_status_check.format(str(i))))
                balance_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.balance_status_check.format(str(i))))
                channel_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.channel_check.format(str(i))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.fps_ref_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.inward_outward_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.deptor_ac_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.creditor_ac_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.amount_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.payment_status_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.balance_status_check.format(str(i)))))
                # print(self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.channel_check.format(str(i)))))

                is_right_amount= False
                is_right_channel= False
                if self.the_str_in_value("Inward",inward_outward_check):
                    is_get_inward_trans =True
                    is_right_amount = "+" in amount_check
                    is_right_channel = self.the_str_in_value("ECFPS",channel_check)
                elif self.the_str_in_value("Outward",inward_outward_check):
                    is_get_outward_trans =True
                    is_right_amount = "-" in amount_check
                    is_right_channel = self.the_str_in_value("boa",channel_check)
                is_right_debtor_ac=self.the_str_in_value(debtor_ac,deptor_ac_check)
                is_right_creditor_ac= self.the_str_in_value(creditor_ac,creditor_ac_check)
                is_right_payment_status= self.the_str_in_value("Accepted",payment_status_check) and self.the_str_in_value("Settled",payment_status_check)
                is_right_balance_statust= self.the_str_in_value("POSTED",balance_status_check)
                result= is_right_amount and is_right_channel and is_right_debtor_ac and is_right_creditor_ac and is_right_balance_statust and is_right_payment_status

                if result:
                    print("The transfer from {} to {}  amout {} is success with {}! ".format(debtor_ac,creditor_ac,amount,inward_outward_check))
                else:
                    if not is_right_amount:print("Amount error!")
                    if not is_right_channel:print("Channel error!")
                    if not is_right_payment_status:print("payment  status error!")
                    if not is_right_balance_statust:print("balance status error!")
                    raise Exception

            else:
                continue

    def the_str_in_value(self,target,value):
        return target.upper() in value.upper()




    def check_record(self):
        fps_ref_check_outward =self.get_ele_attribute_text(ECPFS_Ele.fps_ref_check_outward)
        print("fps_ref_check_outward",fps_ref_check_outward)
        fps_ref_check_inward =self.get_ele_attribute_text(ECPFS_Ele.fps_ref_check_inward)
        print("fps_ref_check_inward",fps_ref_check_inward)
        inward_outward_check_outward =self.get_ele_attribute_text(ECPFS_Ele.inward_outward_check_outward)
        print("inward_outward_check_outward",inward_outward_check_outward)
        inward_outward_check_inward =self.get_ele_attribute_text(ECPFS_Ele.inward_outward_check_inward)
        print("inward_outward_check_inward",inward_outward_check_inward)
        deptor_ac_check_outward =self.get_ele_attribute_text(ECPFS_Ele.deptor_ac_check_outward)
        print("deptor_ac_check_outward",deptor_ac_check_outward)
        deptor_ac_check_inward =self.get_ele_attribute_text(ECPFS_Ele.deptor_ac_check_inward)
        print("deptor_ac_check_inward",deptor_ac_check_inward)
        creditor_ac_check_outward =self.get_ele_attribute_text(ECPFS_Ele.creditor_ac_check_outward)
        print("creditor_ac_check_outward",creditor_ac_check_outward)
        creditor_ac_check_inward =self.get_ele_attribute_text(ECPFS_Ele.creditor_ac_check_inward)
        print("creditor_ac_check_inward",creditor_ac_check_inward)
        amount_check_outward =self.get_ele_attribute_text(ECPFS_Ele.amount_check_outward)
        print("amount_check_outward",amount_check_outward)
        amount_check_inward =self.get_ele_attribute_text(ECPFS_Ele.amount_check_inward)
        print("amount_check_inward",amount_check_inward)
        payment_status_check_outward =self.get_ele_attribute_text(ECPFS_Ele.payment_status_check_outward)
        print("payment_status_check_outward",payment_status_check_outward)
        payment_status_check_inward =self.get_ele_attribute_text(ECPFS_Ele.payment_status_check_inward)
        print("payment_status_check_inward",payment_status_check_inward)
        balance_status_check_outward =self.get_ele_attribute_text(ECPFS_Ele.balance_status_check_outward)
        print("balance_status_check_outward",balance_status_check_outward)
        balance_status_check_inward =self.get_ele_attribute_text(ECPFS_Ele.balance_status_check_inward)
        print("balance_status_check_inward",balance_status_check_inward)
        channel_check_outward =self.get_ele_attribute_text(ECPFS_Ele.channel_check_outward)
        print("channel_check_outward",channel_check_outward)
        channel_check_inward =self.get_ele_attribute_text(ECPFS_Ele.channel_check_inward)
        print("channel_check_inward",channel_check_inward)

    def get_total_page(self):
        self.scroll_windows_to_bottom()
        pages_str =self.get_ele_attribute_text(ECPFS_Ele.items_total_page_xpath)
        print("Tatol page: ",pages_str)
        page = pages_str.split(" ")[5].strip()

        page = int(page)
        if page >100:
            print("Page is large")
            num=100
        else:
            num=page
        self.click_payment_excepption()
        return num

    def click_search_button(self):
        self.click_ele(ECPFS_Ele.search_btn)


    def ecfpc_reg_test_18(self):
        self.get_payment_exception()
        self.click_search_button()
        page=self.get_total_page()
        for i in range(page):
            for i in range(1,11):
                # ref =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_fps_ref_check.format(str(i))))
                status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_status_check.format(str(i))))
                balance_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_balance_status_check.format(str(i))))
                exception_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_exception_status_check.format(str(i))))

                is_right_status= self.the_str_in_value("Accepted",status_check) and self.the_str_in_value("Settled",status_check)
                is_right_balance_statust= self.the_str_in_value("ERROR",balance_status_check)
                is_right_exception_status= self.the_str_in_value("PENDING",exception_status_check)

                if is_right_status and is_right_balance_statust and is_right_exception_status:
                    self.process_exception_item(i)
                else:
                    continue

            self.click_ele(ECPFS_Ele.next_page_btn)


    def check_the_combine_conditions(self,i,balance_status_value,exception_status_value):
        status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_status_check.format(str(i))))
        balance_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_balance_status_check.format(str(i))))
        exception_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_exception_status_check.format(str(i))))

        is_right_status= self.the_str_in_value("Accepted",status_check) and self.the_str_in_value("Settled",status_check)
        # is_right_balance_status= self.the_str_in_value("ERROR",balance_status_check)
        is_right_balance_status= self.the_str_in_value(balance_status_value,balance_status_check)
        is_right_exception_status= self.the_str_in_value(exception_status_value,exception_status_check)

        if is_right_status and is_right_balance_status and is_right_exception_status:
            return True
        else:
            return False

    def check_the_status_value(self,i,value):
        status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_status_check.format(str(i))))
        is_right_status= self.the_str_in_value(value,status_check)
        if is_right_status: return True
        return False


    def ecfpc_reg_test_21(self):
        self.get_payment_exception()
        self.click_search_button()
        page=self.get_total_page()
        for i in range(page):
            for i in range(1,11):
                ref =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_fps_ref_check.format(str(i))))
                # flag =self.check_the_combine_conditions(i,"ERROR","PENDING")
                status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_status_check.format(str(i))))
                balance_status_check =self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_balance_status_check.format(str(i))))
                is_right_status= self.the_str_in_value("PENDING",status_check)
                is_right_balance_status= self.the_str_in_value("UNPOSTED",balance_status_check)
                if is_right_status and is_right_balance_status:
                    self.process_exception_item_21(i,ref)
                else:
                    continue

            self.click_ele(ECPFS_Ele.next_page_btn)


    def open_item_detail(self,check_box_num):
        check_box_path = ECPFS_Ele.item_check_box_path.format(str(check_box_num))
        self.click_ele((By.XPATH,check_box_path))
        self.click_ele(ECPFS_Ele.view_detail_btn)
        self.scroll_windows_to_bottom()

    def process_exception_item_21(self,check_box_num,ref):
        print("ref: ",ref)
        self.open_item_detail(check_box_num)
        self.scroll_windows_to_bottom()
        self.click_Enquire_ICL_Status()
        self.click_payment_detail_close_btn()
        self.scroll_windows_to_top()

        try:
            self.select_fps_no_search()
            self.payment_exception_ref_input(ref)
            self.payment_excepption_all_status_selected()
            self.click_payment_exception_search_btn()
            self.click_ele(ECPFS_Ele.exception_first_item_path)
        except:
            self.select_transaction_id_search()
            self.payment_exception_ref_input(ref)
            self.payment_excepption_all_status_selected()
            self.click_payment_exception_search_btn()
        self.get_status_value_for_search()
        self.get_balance_status_value_for_search()
        self.get_exception_status_value_for_search()

        exception_status_in_payment_detail =self.get_ele_attribute_value(ECPFS_Ele.exception_status_in_payment_detail,'value')
        print('exception_status_in_payment_detail:',exception_status_in_payment_detail)
        if "COMPLETED" in exception_status_in_payment_detail:
            print("exception_status_in_payment_detail pass")

    def get_status_value_for_search(self):
        value = self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_status_check.format(str(1))))
        print("status",value)
        if "Rejected" in value or "" in value:
            return True
        return  False


    def get_balance_status_value_for_search(self):
        value = self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_balance_status_check.format(str(1))))
        print("balance_status",value)
        if "POSTED" in value and  "UNPOSTED" not in value:
            return True
        return  False

    def get_exception_status_value_for_search(self):
        value= self.get_ele_attribute_text((By.XPATH,ECPFS_Ele.exception_exception_status_check.format(str(1))))
        print("exception_status",value)
        if "Rejected" in value or "" in value:
            return True
        return  False

    def select_fps_no_search(self):
        self.click_ele(ECPFS_Ele.fps_ref_no_transaction_drop_btn)
        self.click_ele(ECPFS_Ele.fps_ref_no_item)

    def select_transaction_id_search(self):
        self.click_ele(ECPFS_Ele.fps_ref_no_transaction_drop_btn)
        self.click_ele(ECPFS_Ele.transaction_id_item)

    def click_Enquire_ICL_Status(self):
        self.click_ele(ECPFS_Ele.enquireIclStatusBtn,2)

    def click_payment_detail_close_btn(self):
        self.click_ele(ECPFS_Ele.payment_detail_close_btn)

    def click_payment_exception_search_btn(self):
        self.click_ele(ECPFS_Ele.search_btn)

    def payment_excepption_all_status_selected(self):
        self.click_ele(ECPFS_Ele.payment_exception_status_search_btn)
        self.click_ele(ECPFS_Ele.payment_exception_status_select_all)

    def scroll_windows_to_top(self):
        self.scroll_windows(ECPFS_Ele.scroll_to_top)
        # page = self.driver.find_element_by_xpath('//*[@id="viewPaymentDetailModal"]')
        # for i in range(10):
        #     page.send_keys(Keys.PAGE_UP)
        #     sleep(0.2)

    def scroll_windows_to_bottom(self):
        self.scroll_windows(ECPFS_Ele.scroll_to_bottom)
        #通过找到弹出的窗口，用键盘实现下拉菜单
        # page = self.driver.find_element_by_xpath('//*[@id="viewPaymentDetailModal"]')
        # for i in range(10):
        #     page.send_keys(Keys.PAGE_DOWN)
        #     sleep(0.2)

    def payment_exception_ref_input(self,ref):
        self.input_ele(ECPFS_Ele.payment_excepption_ref_input,ref)

    def reg_temp(self,ref):
        check_ref="ONU202101280000000000000010822"
        print(ref)
        self.get_payment_exception()
        self.payment_exception_ref_input(check_ref)
        self.click_ele(ECPFS_Ele.search_btn)
        self.process_exception_item(1)


    def process_exception_item(self,check_box_num):
        check_box_path = ECPFS_Ele.item_check_box_path.format(str(check_box_num))
        self.click_ele((By.XPATH,check_box_path))
        self.click_ele(ECPFS_Ele.view_detail_btn)
        self.scroll_windows_to_bottom()
        sleep(2)
        payment_result_in_payment_detail =self.get_ele_attribute_value(ECPFS_Ele.payment_result_in_payment_detail,'value')
        print("payment_result_in_payment_detail: ",payment_result_in_payment_detail)

        self.click_ele(ECPFS_Ele.Ledger_tx_status_detail_btn)
        self.click_ele(ECPFS_Ele.reprocess_redger_transaction_btn,5)

        self.scroll_windows_to_bottom()
        Ledger_tx_status_ref =self.get_ele_attribute_value(ECPFS_Ele.ledger_tx_status_value,'value')
        ledger_tx_status_value =self.get_ele_attribute_value(ECPFS_Ele.ledger_tx_status_value,'value')
        if "POSTED" in ledger_tx_status_value:
             print("ledger_tx_status_value  PASS")
        else:
             print("ledger_tx_status_value  Fail")
             # self.scroll_windows(ECPFS_Ele.scroll_to_bottom)
             self.click_ele(ECPFS_Ele.redger_transaction_close_single)

        payment_result_in_payment_detail =self.get_ele_attribute_value(ECPFS_Ele.payment_result_in_payment_detail,'value')
        is_right_payment_status= self.the_str_in_value("Accepted",payment_result_in_payment_detail) and self.the_str_in_value("Settled",payment_result_in_payment_detail)
        print('payment_result_in_payment_detail:',payment_result_in_payment_detail)
        if is_right_payment_status:
            print("payment_result_in_payment_detail pass")

        exception_status_in_payment_detail =self.get_ele_attribute_value(ECPFS_Ele.exception_status_in_payment_detail,'value')
        print('exception_status_in_payment_detail:',exception_status_in_payment_detail)
        if "COMPLETED" in exception_status_in_payment_detail:
            print("exception_status_in_payment_detail pass")

        self.click_ele(ECPFS_Ele.Ledger_tx_status_detail_btn)

        redger_transaction_details_status =self.get_ele_attribute_value(ECPFS_Ele.redger_transaction_details_status,'value')
        print('redger_transaction_details_status:',redger_transaction_details_status)
        if "SUCCESS" in redger_transaction_details_status:
             print("redger_transaction_details_status  PASS")

        redger_transaction_details_type =self.get_ele_attribute_value(ECPFS_Ele.redger_transaction_details_type,'value')
        print('redger_transaction_details_type:',redger_transaction_details_type)
        if "CREDIT" in redger_transaction_details_type:
             print("redger_transaction_details_type  PASS")

        redger_transaction_details_ref =self.get_ele_attribute_value(ECPFS_Ele.redger_transaction_details_ref,'value')
        print('redger_transaction_details_ref:',redger_transaction_details_ref)
        if Ledger_tx_status_ref in redger_transaction_details_ref:
            print("redger_transaction_details_ref PASS")

        redger_transaction_details_amount =self.get_ele_attribute_value(ECPFS_Ele.redger_transaction_details_amount,'value')
        print('redger_transaction_details_amount:',redger_transaction_details_amount)
        if  redger_transaction_details_amount is not None and "." in redger_transaction_details_amount:
             print("redger_transaction_details_amount  PASS")

        self.scroll_windows_to_bottom()
        self.click_ele(ECPFS_Ele.redger_transaction_close_btn)
        self.click_ele(ECPFS_Ele.view_journal_btn)

        request_event_log_status =self.get_ele_attribute_value(ECPFS_Ele.request_event_log_status)
        print('request_event_log_status:',request_event_log_status)
        if "SUCCESS" in request_event_log_status:
             print("request_event_log_status  PASS")

