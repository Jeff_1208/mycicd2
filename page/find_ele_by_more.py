#! /usr/bin/env pyhton
# -*- coding:utf-8 -*-
# author:Jeff.xie
# datetime:2023/8/10 10:29
# software:PyCharm


from time import sleep
from typing import Any, List

from pythium import find_by, find_all, by, ios_find_by, android_find_by, ios_find_all, android_find_all
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from pythium import Page
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from appium.webdriver.webelement import WebElement as MobileElement


class Baidu(Page):

    def _is_loaded(self):
        pass

    driver = None

    def __init__(self, driver):
        self.driver = driver
        Page.__init__(self, self.driver)

    def click_element(self, ele: WebElement, sleep_time=0.2):
        try:
            ele.click()
            sleep(sleep_time)
        except Exception as e:
            print('except:', e)
            raise e

    def input_ele_by_ele(self, ele: WebElement, text):
        ele.clear()
        ele.send_keys(text)

    def wait_until_element_visible(self, ele: WebElement, timeout=30, poll=0.5):
        el = None
        try:
            # EC.visibility_of_element_located
            wait = WebDriverWait(self.driver, timeout, poll)
            el = wait.until(EC.visibility_of_element_located(ele))
        except Exception as e:
            print("can not find the element: " + str(ele))
            print('except:', e)
            raise e
        return el

    # def is_ele_visible(self, ele: WebElement, timeout=30) -> bool:
    def is_ele_visible(self, ele, timeout=30):
        try:
            print("start to find the element: " + str(ele))
            # self.wait_until_element_visible(ele, timeout)
            # wait = WebDriverWait(self.driver, timeout, 0.5)
            # wait.until(EC.visibility_of(ele))
            v = WebDriverWait(self.driver, timeout, 0.5).until(EC.visibility_of(ele))
            return True
        except Exception as e:
        # except:
            print("can not find the element: " + str(ele))
            # print('except:', e)
            raise e
            return False

    def is_element_visible(self, loc, timeout=30):
        # 要判断一个元素是否存在，传入的参数是loc，即 (By.XPATH, '//*[@id="testtest111"]/span')
        # 而不是
        try:
            print("start to find the element: " + str(loc))
            WebDriverWait(self.driver, timeout, 0.5).until(EC.visibility_of(loc))
            return True
        except:
            print("can not find the element: " + str(loc))
            return False

    def aa(self, loc, timeout=30):
        assert(WebDriverWait(self.driver, timeout, 0.5).until(EC.visibility_of(loc)))

    def get_screenshot(self, pic_name):
        """
        :param pic_name: 截图名字
        :return:
        """
        full_path = pic_name + ".png"
        return self.driver.get_screenshot_as_file(full_path)

    def open_url(self, url="https://blog.csdn.net"):
        self.driver.get(url)
        sleep(5)

    @find_by(css=".search")
    @ios_find_by(ios_predicate='value == "Search something"')
    @android_find_by(android_uiautomator='resourceId("com.app:id/search_txtbox")')
    def search_input(self) -> WebElement:
        ...

    @property
    @find_by(css=".search")
    @ios_find_by(ios_predicate='value == "Search something"')
    @android_find_by(android_uiautomator='resourceId("com.app:id/search_txtbox")')
    def search_input_with_property(self) -> WebElement:
        ...

    @property
    @find_all(by(css=".icon-logo1"), by(css=".icon-logo"))
    def find_all_web_test(self) -> WebElement:
        return Any

    @property
    @ios_find_all(by(ios_predicate='value == "Search something"'), by(ios_predicate='value == "Search result"'))
    @android_find_all(by(android_uiautomator='resourceId("com.app:id/search_txtbox")'),
                      by(android_uiautomator='resourceId("com.app:id/search_txtbox")'))
    def find_all_mobile_test(self) -> WebElement:
        return Any

    # for dynamical locator
    @find_by(xpath="//div[{n}]/a[{k}]/div[{m}]/{f}")
    @ios_find_by(xpath="//div[1]/a[{n}]/div[{k}]")
    def dynamical_locator(self, n, k, m=4, f=6) -> WebElement:
        ...

    # for list WebElements
    @find_by(css=".login")
    def list_web_elements(self) -> List[MobileElement]:
        ...

    @property
    @find_all(by(css='//*[@id="csdn-toolbar"]/div/div/div[3]/div/div[1]/a'), by(class_name='toolbar-btn-loginfun'))
    def csdn_login(self) -> WebElement:
        ...

    @property
    @find_by(xpath='//*[@id="passportbox"]/span')
    def close_item(self) -> WebElement:
        ...

    @property
    @find_by(xpath='//*[@id="testtest111"]/span')
    def test_item(self) -> WebElement:
        ...

    def login(self):
        self.click_element(self.csdn_login)
        # self.get_screenshot("login")
        sleep(2)
        self.aa(self.test_item)
        if self.is_element_visible((By.XPATH, '//*[@id="testtest111"]/span'),3):
            self.click_element(self.test_item)
        # self.click_element_if_exists(self.test_item, 3)
        sleep(1)
        # self.get_screenshot("last_page")
        # self.click_element_if_exists(self.close_item, 3)
        # self.click_element_if_exists(self.allow_button, 3)
        sleep(5)

    def check_alert(self):
        self.driver.switch_to.alert()
        sleep(1)
        self.click_element(self.allow_button)

    def click_element_if_exists(self, ele, timeout=10):
        try:
            self.is_ele_visible(ele, timeout)
            self.click_element(ele)
        except Exception as E:
            pass

    # selenium 新的定位元素的方法
    @find_by(xpath='//*[@id="search"]/form/div[3]/input')
    def search_button_element(self) -> WebElement:
        ...

    @find_by(xpath='//*[@id="passportbox"]/img')
    def close_img(self) -> WebElement:
        ...

    @find_by(xpath='//*[@text="允许"]')
    def allow_button(self) -> WebElement:
        ...

    def main_step(self):

        self.driver.maximize_window()
        self.open_url()
        self.login()
        print("Finished!!!!")


if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    # options.add_argument("--headless")
    driver = webdriver.Chrome(options=options)
    baidu = Baidu(driver)
    baidu.main_step()
