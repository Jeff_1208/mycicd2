__author__ = 'Jeff.xie'

from selenium.webdriver.common.by import By
from base.app_base import AppBase
from time import sleep


class Bank(AppBase):

    def __init__(self,driver):
        self.driver = driver

    login= (By.XPATH, "//android.widget.TextView[@text='Login']")
    def click_login(self):
        self.click_ele(self.login)

    eyeBtn= (By.XPATH, "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[4]")
    rocketButton= (By.XPATH, "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]")
    # skipRoot= (By.XPATH, "//android.widget.TextView[contains(@text,'Root')]/../android.widget.Switch")
    skipRoot= (By.XPATH, "//android.widget.TextView[contains(@text,'Jailbreak')]/../android.widget.Switch")
    skipORC= (By.XPATH, "//android.widget.TextView[contains(@text,'OCR')]/../android.widget.Switch")
    skipRebindDevice= (By.XPATH, "//android.widget.TextView[contains(@text,'Rebind Device')]/../android.widget.Switch")
    devPanelBackBtn= (By.XPATH, "//*[@text='Dev Panel']/../android.widget.TextView[1]")
    devPanelTitle= (By.XPATH, "//*[@text='Dev Panel']")
    "//*[contains(@text,’name‘)]"
    "//*[@text=’name‘]"

    def skip_root(self):
        self.wait_util_element_is_visible(self.login)
        self.click_ele(self.eyeBtn)
        self.click_ele(self.rocketButton)
        self.wait_util_element_is_visible(self.devPanelTitle)
        self.scrollup_until_element_clickable(self.skipRoot)
        self.setSwithToChecked(self.skipORC)
        self.setSwithToChecked(self.skipRebindDevice)
        self.setSwithToChecked(self.skipRoot)
        self.click_ele(self.devPanelBackBtn)

    gotItBtn= (By.XPATH, "//*[contains(@text,'Got it')]")
    # gotItBtn= (By.ID, "-btn")
    def click_got_it(self):
        self.click_ele(self.gotItBtn)

    upgradeOKBtn= (By.XPATH, "//android.widget.TextView[@text='Got it']")
    exploreText= (By.XPATH, "//android.widget.TextView[@text='Select one you want to explore now!']")


    userNameTxt= (By.XPATH, "//android.widget.TextView[@text='Username']")
    userNameInput= (By.ID, "username-input")
    passwordTxt= (By.XPATH, "//android.widget.TextView[@text='Password']")
    passwordInput= (By.ID, "password-input")
    welcomeBack= (By.XPATH,"//*[@text='Welcome back']" )
    loginButton= (By.XPATH,"//android.widget.TextView[@text='Login']" )
    def bank_input_login(self,username,pw):
        self.click_ele(self.userNameTxt)
        self.input_app_ele(self.userNameInput,username)
        self.click_ele(self.passwordTxt)
        self.input_app_ele(self.passwordInput,pw)
        self.click_ele(self.loginButton)

    def welab_bank_login_001(self,username,pw):
        self.skip_root()
        self.click_login()
        self.click_got_it()
        self.bank_input_login(username,pw)
        sleep(20)



