__author__ = 'Jeff.xie'
from appium import webdriver
# coding:utf-8
from time import sleep

from appium.webdriver.common.multi_action import MultiAction
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from appium.webdriver.common.appiumby import AppiumBy

# pip install Appium-Python-Client
desired_caps = {}

desired_caps['platformName'] = 'Android'
# desired_caps['platformVersion'] = '11'
# desired_caps['deviceName'] = 'RFCNA0DREDX'
desired_caps['platformVersion'] = '11'
desired_caps['deviceName'] = '9b2157cfaedb'
desired_caps['appPackage'] = 'welab.bank.mobile.stage'
desired_caps['appActivity'] = 'com.welabfrontend.MainActivity'
driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub",desired_caps)
# driver.set_location(12, 123, 10)  #设置经度，纬度，海拔
print(driver.location)

# Java
# driver.setLocation(new Location(49, 123, 10)); // Must be a driver that implements LocationContext
# 参考文档
# https://appium.readthedocs.io/en/stable/en/commands/session/geolocation/set-geolocation/#example-usage

sleep(10)

# driver.find_element(By.XPATH,"//android.widget.TextView[@text='登入']").click()
# driver.find_element_by_android_uiautomator('new UiSelector().text("登入")').click() # 不可用
driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,'new UiSelector().text("登入")').click()  # 可用
sleep(1)
# driver.find_element(By.XPATH,"//android.widget.TextView[@text='用戶名稱']").click()
sleep(1)
driver.find_element(By.XPATH,'//android.widget.EditText[@content-desc="username-input"]').send_keys('contester0042')
sleep(1)
driver.find_element(By.XPATH,"//android.widget.TextView[@text='密碼']").click()
sleep(1)
driver.find_element(By.XPATH,'//android.widget.EditText[@content-desc="password-input"]').send_keys('Aa123321')
# driver.find_element(By.XPATH,'//android.widget.EditText[@content-desc="password-input"]').send_keys(Keys.ENTER)
driver.hide_keyboard()
sleep(1)
driver.find_element(By.XPATH,"//android.widget.TextView[@text='登入']").click()
sleep(10)

def getSize(driver):
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']
    return x, y

# 放大
def narrow():
    x, y = getSize(driver)
    action1 = TouchAction(driver)
    action2 = TouchAction(driver)
    zoom_action = MultiAction(driver)
    action1.press(x=x*0.2, y=y*0.2).wait(1000).move_to(x=x*0.4, y=y*0.4).wait(1000).release()
    action2.press(x=x*0.8, y=y*0.8).wait(1000).move_to(x=x*0.6, y=y*0.6).wait(1000).release()
    zoom_action.add(action1, action2)
    zoom_action.perform()

# 缩小
def enlarge():
    x, y = getSize(driver)
    action1=TouchAction(driver)
    action2=TouchAction(driver)
    zoom_action=MultiAction(driver)
    action1.press(x=x*0.4,y=y*0.4).wait(1000).move_to(x=x*0.2,y=y*0.2).wait(1000).release()
    action2.press(x=x*0.6,y=y*0.6).wait(1000).move_to(x=x*0.8,y=y*0.8).wait(1000).release()
    zoom_action.add(action1,action2)
    zoom_action.perform()

# 放大 / 缩小操作另一个方法：zoom / pinch
# 参数：percent：在某控件上执行放大操作，默认放大比例为200 %，steps：表示 放大 / 缩小 动作分多少步完成，默认50
# zoom(self, element=None, percent=200, steps=50)
# pinch(self, element=None, percent=200, steps=50)

# 放大
# driver.zoom(element)
# 缩小
# driver.pinch(element)

# driver.scroll(ele1, ele2) #从元素1滑到到元素2


