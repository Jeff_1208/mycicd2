#! /usr/bin/env pyhton
# -*- coding:utf-8 -*-
# author:jeff.xie
# datetime:2024/1/29 17:18
# software:PyCharm
import json

import requests


class Weather:

    def __init__(self):
        pass

    def get_weather(self, city: str):
        url = "http://apis.juhe.cn/simpleWeather/query?city={}&key=ee1edc98bcf929410042a1fc1f764c9f".format(city)
        data = requests.get(url=url)
        print(type(data))
        return data

    def read_json(self,json_string, flag):
        # a = json.dumps(json_string)
        # a = json.dumps(json_string, sort_keys=True)
        b = json.loads(json_string)
        c = b.items()
        for key, v in c:
            if key == flag:
                return v
            if type(v) == dict:
                self.read_json(v, flag)

if __name__ == '__main__':
    w = Weather()
    r = w.get_weather("上海")
    data = r.text
    print(r.headers)
    print(type(r.headers))
    print(data)
    print(type(data))
    value = w.read_json(data, "reason")
    print(value)
