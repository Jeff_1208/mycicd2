from selenium.webdriver.common.by import By

__author__ = 'Jeff.xie'


class Locations:

    #点击高级
    gaoji_btn = (By.XPATH, '//*[@id="details-button"]')
    going_link = (By.XPATH, '//*[@id="proceed-link"]')

    #点击login按钮，进入登录页面，输入用户名密码
    login_btn = (By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li')
    login_username = (By.XPATH, '//*[@id="username"]')
    login_pw = (By.XPATH, '//*[@id="password"]')
    #登录提交按钮
    login_submmit_btn = (By.XPATH, '//*[@id="kc-login"]')


    # logout点击的两个元素
    logout_btn_1 = (By.XPATH, '//*[@id="navbarDropdown"]')
    logout_btn_2 = (By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li/div/button')


    #百度搜索
    baidu_input = (By.XPATH, '//*[@id="kw"]')
    baidu_btn = (By.XPATH, '//*[@id="su1"]')

