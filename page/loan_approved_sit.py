__author__ = 'Jeff.xie'
# encoding:utf-8

from time import sleep
from base.base import Base
from page.element_location import Locations
import page


class Loan_sit(Base):

    def __init__(self,driver):
        #初始化Base方法，并且把driver传递给base  实例化
        Base.__init__(self,driver)  #  特别注意!!!!!!

    def open_url(self,url="https://sit-ploan-portal.dev-bks.welabts.net/"):
        self.driver.get(url)

    def click_gao(self):
        try:
            sleep(2)
            self.click_ele(Locations.gaoji_btn)# 点击高级
            self.click_ele(Locations.going_link)# 点击进入链接
            self.driver.implicitly_wait(10)
        except Exception as e:
            print("click gaoji exception!!!")
            pass

    def login(self,username,pw):
        self.click_ele(Locations.login_btn)
        self.click_gao()
        self.input_ele(Locations.login_username,username)
        self.input_ele(Locations.login_pw,pw)
        self.click_ele(Locations.login_submmit_btn)

    def logout(self):
        self.click_ele(Locations.logout_btn_1)
        self.click_ele(Locations.logout_btn_2)


