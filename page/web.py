#! /usr/bin/env pyhton
# -*- coding:utf-8 -*-
# author:jeff.xie
# datetime:2024/1/31 11:37
# software:PyCharm

from time import sleep

from pythium import find_by
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
url="https://www.baidu.com"
url="https://blog.csdn.net"
url="file:///D:/2/AlertPage.html"
driver.get(url)
driver.maximize_window()
sleep(5)
print("KKKKKKKKK")
sleep(3)
driver.find_element(By.XPATH, '//*[@id="alert"]').click()
sleep(1)
t = driver.switch_to.alert
print(t.text)
sleep(1)
# t.accept()
t.dismiss()  #关闭alert
sleep(2)
driver.close()

# 出现报错: TypeError: ‘Alert‘ object is not callable
# 原因分析： ‘Alert’ object is not callable 的含义为Alert不能被函数调用，它不是一个函数。
# 解决方法： 将Alert后的括号去掉


# 本地html文件的内容 复制到txt，然后保存为html格式
"""
<html>  

<head>  
   <title>Alert</title>  
</head>  

<body>  
<input id = "alert" value = "alert" type = "button" onclick = "alert('您关注了yoyoketang吗？');"/>  
<input id = "confirm" value = "confirm" type = "button" onclick = "confirm('确定关注微信公众号：yoyoketang？');"/>  
<input id = "prompt" value = "prompt" type = "button" onclick = "var name = prompt('请输入微信公众号:','yoyoketang'); document.write(name) "/>    
</body>   

</html>  
"""