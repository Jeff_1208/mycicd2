__author__ = 'Jeff.xie'
# encoding:utf-8


from page.loan_approved_sit import Loan_sit
from page.loan_stage import Loan
from page.baidu import Baidu
from page.ecfps import ECFPS
from page.ECFPS_Reg import ECFPS_Reg
from page.bank_app import Bank

class Page_Object():

    def __init__(self,driver):
        self.driver = driver

    def get_loan_sit(self):
        return Loan_sit(self.driver)

    def get_baidu(self):
        return Baidu(self.driver)

    def get_ecfps(self):
        return ECFPS(self.driver)

    def get_loan_stage(self):
        return Loan(self.driver)

    def get_ecfps_reg(self):
        return ECFPS_Reg(self.driver)

    def get_bank(self):
        return Bank(self.driver)
