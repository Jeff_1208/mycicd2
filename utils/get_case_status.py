#! /usr/bin/env pyhton
# -*- coding:utf-8 -*-
# author:jeff.xie
# datetime:2024/2/2 14:03
# software:PyCharm


import json
import os

def get_case_name_status(path):
    # 读取json文件
    with open(path, 'r', encoding='utf-8') as f:
        data = json.load(f)

    # 解析json数据
    function_name = data["name"]
    print(function_name)

    status = data["testStage"]["status"]   # 解析 testStage对应json内容，取status对应的值
    case_name = data["testStage"]["steps"][0]["name"]
    # 解析 testStage对应json内容，取steps对应的json数据的第一个json数据，取name对应的值
    print(status)
    print(case_name)


def get_all_case_status(path):
    files = os.listdir(path)
    print(path)
    for f in files:
        # f 是文件名称
        full_path = os.path.join(path,f)
        print(f)
        print(full_path)
        get_case_name_status(full_path)





if __name__ == '__main__':
    dir_path='../report1/report_2024_02_02_10_03_10/html/data/test-cases'
    get_all_case_status(dir_path)
