__author__ = 'Jeff.xie'

from PIL import ImageGrab
from win32api import GetSystemMetrics
import numpy as np
import cv2


class Vedio():
    def recording_vedio(self):
        num,fps,width,heigh=0,12,GetSystemMetrics(0),GetSystemMetrics(1) #获得分辨率
        size=(width,heigh)
        video=cv2.VideoWriter("1.avi", cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, size)
        while 1:
            num+=1
            bbox=(0,0,width,heigh)#四个参数代表了开始截图的x,y，结束截图的x,y，后两个可以看电脑
            im=ImageGrab.grab(bbox)
            video.write(np.array(im))#将img convert ndarray
            if num>100:#这个可以控制时间结束，日后改成一个按钮
                video.release()
                break

if __name__=='__main__':
    v = Vedio()
    v.recording_vedio()
