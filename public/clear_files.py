__author__ = 'Jeff.xie'


import os
import shutil

def remove_json_file():
    os.chdir("./report1")
    fs = os.listdir("./")
    for f in fs:
        if os.path.isfile(f):
            # print(f)
            os.remove(f)
    os.chdir(r"..")
# remove_json_file()


def remove_report_folder():
    os.chdir("./report1")
    fs = os.listdir("./")
    folder_list=[]
    for f in fs:
        if os.path.isdir(f):
            folder_list.append(f)
    if len(folder_list)>5:
        for i in range(len(folder_list)-5):
            print("remove ./{}".format(folder_list[i]))
            shutil.rmtree(r"./{}".format(folder_list[i]))
    os.chdir(r"..")
# remove_report_folder()


def remove_log_files():
    num = 5
    os.chdir("./logs")
    fs = os.listdir("./")
    logs_file_list=[]
    for f in fs:
        if os.path.isfile(f):
            logs_file_list.append(f)
    if len(logs_file_list)>num:
        delete_file=logs_file_list[0:len(logs_file_list)-num]
    for f in delete_file:
        if os.path.isfile(f):
            print("remove ./{}".format(f))
            os.remove(f)
    os.chdir(r"..")

# remove_log_files()







