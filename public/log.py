#!/usr/bin/env python
# _*_ coding:utf-8 _*_


import logging, time
import os,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
from config import setting

# 鏃ュ織瀛樻斁鏂囦欢澶癸紝濡備笉瀛樺湪锛屽垯鑷姩鍒涘缓涓�涓猯ogs鐩綍
if not os.path.exists(setting.LOG_DIR):os.mkdir(setting.LOG_DIR)

class Log():
    """
    鏃ュ織璁板綍绫�
    """
    def __init__(self):
        # 鏂囦欢鐨勫懡鍚�
        # self.logname = os.path.join(setting.LOG_DIR, '%s.log'%time.strftime('%Y-%m-%d %H_%M_%S'))
        self.logname = os.path.join(setting.LOG_DIR, '%s.log'%time.strftime('%Y_%m_%d_%H_%M_%S'))
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        # 鏃ュ織杈撳嚭鏍煎紡
        self.formatter = logging.Formatter('[%(asctime)s] [%(filename)s|%(funcName)s] [line:%(lineno)d] %(levelname)-8s: %(message)s')

    def __console(self, level, message):
        # 鍒涘缓涓�涓狥ileHandler锛岀敤浜庡啓鍒版湰鍦版棩蹇楁枃浠�
        fh = logging.FileHandler(self.logname,encoding='utf-8')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(self.formatter)
        self.logger.addHandler(fh)

        # 鍒涘缓涓�涓猄treamHandler,鐢ㄤ簬杈撳嚭鍒版帶鍒跺彴
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(self.formatter)
        self.logger.addHandler(ch)

        if level == 'info':
            self.logger.info(message)
        elif level == 'debug':
            self.logger.debug(message)
        elif level == 'warning':
            self.logger.warning(message)
        elif level == 'error':
            self.logger.error(message)
        # 杩欎袱琛屼唬鐮佹槸涓轰簡閬垮厤鏃ュ織杈撳嚭閲嶅闂
        self.logger.removeHandler(ch)
        self.logger.removeHandler(fh)
        # 鍏抽棴鎵撳紑鐨勬枃浠�
        fh.close()

    def debug(self, message):
        self.__console('debug', message)

    def info(self, message):
        self.__console('info', message)

    def warning(self, message):
        self.__console('warning', message)

    def error(self, message):
        self.__console('error', message)
