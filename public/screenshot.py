#!/usr/bin/env python
# _*_ coding:utf-8 _*_
__author__ = 'YinJia'


import os,sys
from time import sleep
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


def insert_img(driver,file_name):
    """
    截图
    :param driver: 启动浏览器
    :param file_name: 截图路径以及文件名
    :return: 返回指定路径的截图文件
    """
    return  driver.get_screenshot_as_file(file_name)

