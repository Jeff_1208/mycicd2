__author__ = 'Jeff.xie'


from base.base import Base
from base.driver_init import driver_init
from page.page import Page_Object
from data.data import Data
import pytest
import allure

'''
1.在测试类中导入driver_init，并且赋值给self.driver
2.在测试类中到入Page_Object,所有的Page也返回对象的方法都在这个类中，driver通过构造对象赋值给某一个page类Loan
    def get_loan_stage(self):
        return Loan(self.driver)
3.page类Loan继承Base类，调用base的__init__方法，这样就把driver赋值给Base
class Loan(Base):
    def __init__(self,driver):
        Base.__init__(driver)
4.同时Loan又继承了Base类，所以可以调用Base的方法，这就保证了driver在每个文件都可用
class Base():
    def __init__(self, driver):
        self.driver = driver
'''


class Test_Loan_Approved_Stage:
    Loan_ID="P20201013387717"
    status=""
    loan_amount=0
    loans_ID_list=[]


    @pytest.fixture()
    def get_loans_id(self):
        return Data().read_loans_numbers()


    @classmethod

    def setup_class(self):
        self.driver = driver_init()
        self.loan = Page_Object(self.driver).get_loan_stage()


    @pytest.mark.run(order=1)
    @pytest.mark.parametrize("user,pw",[("checker1","Aa123456")])
    @pytest.mark.usefixtures("get_loans_id")
    def test_pre_step(self,user,pw,get_loans_id):
        self.loan.open_url()
        self.loan.click_gaoji()
        self.loan.click_login_btn()
        self.loan.click_gaoji()
        self.loan.login_stage(user,pw)
        self.loans_ID_list = get_loans_id
        print("kkkkkkkkkkk: ",self.loans_ID_list)
        for id in self.loans_ID_list:
            self.status=self.loan.get_loans_status(id)
        self.loan.click_detail()
        self.loan_amount=self.loan.get_loan_amout()
        print("loan_amount: ",self.loan_amount)

    @pytest.mark.run(order=2)
    @pytest.mark.skipif("Pending Review" not in status,reason="该步骤已完成")
    @pytest.mark.parametrize("name,user,pw,select_value",[("maker1 maker1 (Maker)","maker1","Aa123456","makerCompleted")])
    def test_first_approved(self,name,user,pw,select_value):
        if "Pending Review" in self.status:
            self.loan.assign_owner(name)
            self.loan.logout()
            self.loan.click_login_btn()
            self.loan.login_stage(user,pw)
            self.loan.first_approved(select_value,self.Loan_ID)
            self.status=self.loan.get_loans_status(self.Loan_ID)

    # self.select_status(driver, "checkerCompleted")
    @pytest.mark.run(order=3)
    @pytest.mark.skipif("Pending Checker" not in status,reason="该步骤已完成")
    @pytest.mark.parametrize("name,user,pw,select_value",[("checker1 checker1 (Checker)","checker1","Aa123456","checkerCompleted")])
    def test_second_approved(self,name,user,pw,select_value):
        if "Pending Checker" in self.status:
            self.loan.logout()
            self.loan.login_stage(user,pw)
            self.loan.assign_owner(name)
            self.loan.second_approved(select_value,self.ID)
            self.status=self.loan.get_loans_status(self.Loan_ID)


    @pytest.mark.run(order=4)
    @pytest.mark.skipif(condition="Pending Approval" in status,reason="该步骤已完成")
    @pytest.mark.parametrize("name_a,name_d,user_a,user_d,user_checker,pw",[("CreditA Tech (Credit Checker A)","CreditD Tech (Credit Checker D)","ta","td","checker1","Aa123456")])
    def test_third_approved(self,name_a,name_d,user_a,user_d,user_checker,pw):
        if "Pending Approval" in self.status:
            if self.loan_amount >=400000:
                self.loan.assign_owner(name_a)
                self.loan.logout()
                self.loan.login_stage(user_a,pw)
                self.loan.third_approved(self.ID)
                self.loan.logout()
                self.loan.login_stage(user_checker,pw)
            self.loan.assign_owner(name_d)
            self.loan.logout()
            self.loan.login_stage(user_d,pw)
            self.loan.third_approved(self.ID)
            self.loan.check_loans_result(user_checker,pw,self.Loan_ID)
            self.loan.driver.quit()




