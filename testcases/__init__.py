__author__ = 'Jeff.xie'

import os
import time
import shutil
# from po_frame.public import  clear_files
import allure
import pytest

def get_project_path():
    project_path=os.getcwd()
    print("KKKKKKKKKKKK")
    print("project_path:"+project_path)
    print("KKKKKKKKKKKK")
    print(type(project_path))
    return project_path

def remove_json_file(path):
    os.chdir(path)
    fs = os.listdir("./")
    for f in fs:
        if os.path.isfile(f):
            # print(f)
            os.remove(f)
    os.chdir(r"..")
# remove_json_file()


def remove_report_folder(path):
    os.chdir(path)
    fs = os.listdir("./")
    folder_list=[]
    for f in fs:
        if os.path.isdir(f):
            folder_list.append(f)
    if len(folder_list)>5:
        for i in range(len(folder_list)-5):
            print("remove ./{}".format(folder_list[i]))
            shutil.rmtree(r"./{}".format(folder_list[i]))
    os.chdir(r"..")
# remove_report_folder()


def remove_log_files():
    num = 5
    os.chdir("./logs")
    fs = os.listdir("./")
    logs_file_list=[]
    for f in fs:
        if os.path.isfile(f):
            logs_file_list.append(f)
    if len(logs_file_list)>num:
        delete_file=logs_file_list[0:len(logs_file_list)-num]
    for f in delete_file:
        if os.path.isfile(f):
            print("remove ./{}".format(f))
            os.remove(f)
    os.chdir(r"..")


def setup_module():
    """    整个.模块只在开始时执行一次    """
    # clear_files.remove_report_folder()
    # clear_files.remove_json_file()
    # clear_files.remove_log_files()
    get_project_path()
    remove_report_folder("./report1")
    remove_json_file("./report1")
    # with open("D:/suite1.txt","a") as f:
    #     f.write("testsuite")

def teardown_module():
    """    整个.模块只在结束时执行一次    """
    time.sleep(2)
    now_str=time.strftime("%Y_%m_%d_%H_%M_%S",time.localtime(time.time()))
    report_folder = "report_"+now_str
    time.sleep(5)
    # os.system("cd D:/Program/MyPythonWorkSpace/MyPythonPlatform/automationplatformpython/report1")
    # os.system("cd ./report1")
    os.chdir(r"./report1")
    os.mkdir(report_folder)
    time.sleep(0.8)
    os.chdir(r"..")
    # os.system("allure generate ./report1 -o ./report1/html/ --clean")
    # os.system("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))
    print("report_folder: ",report_folder)
    os.popen("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))   #成功

    def setup_method(self):
        print("start method case!!!")

    def teardown_method(self):
        print("end method case!!!")