__author__ = 'Jeff.xie'

import pytest
from page.page import Page_Object
from base.driver_init import driver_init


class Test_ECFPS:

    def setup_class(self):
        self.driver = driver_init()
        self.ecfps  = Page_Object(self.driver).get_ecfps()

    def teardown_class(self):
        self.driver.quit()

    @pytest.mark.run(order=1)
    def test_open(self):
        self.ecfps.open_url()

    @pytest.mark.run(order=2)
    @pytest.mark.parametrize("username,pw",[("root","changeit")])
    def test_login(self,username,pw):
        self.ecfps.login(username,pw)

    @pytest.mark.run(order=3)
    @pytest.mark.parametrize("date",["2021-5-13"])
    def test_search(self,date):
        self.ecfps.search_item(date)

    @pytest.mark.run(order=4)
    def test_approved(self):
        self.ecfps.approved("value")

    # @pytest.mark.run(order=5)
    # def test_click_all(self):
    #     self.ecfps.approved("value")

