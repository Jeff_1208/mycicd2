__author__ = 'Jeff.xie'


import pytest
from page.page import Page_Object
from base.driver_init import driver_init
from time import sleep

# Debtor A/C 1000452549
# Creditor A/C 1000497283
class Test_ECFPS:
    debtor_ac  ="1000452549"
    creditor_ac  ="1000497283"  #收钱
    approved_user="jeff.xie"
    approved_pw = "P@ssw0rd"
    # debtor_name  ="LI Ngon Chunk"
    creditor_name  ="LI Ngon Chunk"
    amount=str(15)
    welab_creditor_agent_value='390'
    creditor_agent_672_value='672'
    payment_categary_value="Salary and Benefits Payment"

    def setup_class(self):
        self.driver = driver_init()
        self.ecfps_reg  = Page_Object(self.driver).get_ecfps_reg()
        self.ecfps_reg.open_url()
        self.ecfps_reg.login_first("root","changeit")

    def teardown_class(self):
        pass
        # self.driver.quit()

    # @pytest.mark.run(order=1)
    # @pytest.mark.parametrize("username,pw",[("jeff.xie","P@ssw0rd")])
    # def test_ecfps_reg_11(self,username,pw):
    #     self.ecfps_reg.input_credit_transfer(self.debtor_ac,self.amount,self.payment_categary_value)
    #     self.ecfps_reg.input_creditor_transfer(self.creditor_ac,self.creditor_name,self.welab_creditor_agent_value)   #"Welab Bank Limited (390)"
    #     self.ecfps_reg.logout()
    #     self.ecfps_reg.login(username,pw)
    #     self.ecfps_reg.trans_approved()
    #
    # @pytest.mark.run(order=2)
    # def test_ecfps_reg_11_result(self):
    #     sleep(5)
    #     self.ecfps_reg.click_payment_enquiry()
    #     self.ecfps_reg.check_record_by_ac(self.debtor_ac,self.creditor_ac,self.amount)

    @pytest.mark.run(order=3)
    def test_ecfps_reg_temp(self):
        self.ecfps_reg.ecfpc_reg_test_21()

    # @pytest.mark.run(order=3)
    # @pytest.mark.parametrize("proxy_id,customet_id,accunt_id,customer_name",[("799176979","8000027934","1000452549","LI Ngon Chunk")])
    # def test_ecfps_reg_22(self,proxy_id,customet_id,accunt_id,customer_name):
    #     pass




