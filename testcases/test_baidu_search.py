__author__ = 'Jeff.xie'

import os
import time
from time import sleep

import pytest
import allure

from page.page import Page_Object
from base.driver_init import driver_init
from public import screenshot, clear_files
from base.base import Base


def setup_module():
    """    整个.模块只在开始时执行一次    """
    clear_files.remove_report_folder()
    clear_files.remove_json_file()
    # clear_files.remove_log_files()

def teardown_module():
    """    整个.模块只在结束时执行一次    """
    time.sleep(2)
    """
    now_str=time.strftime("%Y_%m_%d_%H_%M_%S",time.localtime(time.time()))
    report_folder = "report_"+now_str
    time.sleep(5)
    os.system("cd C:/Users/jeff.xie/PycharmProjects/MyProject/po_frame/report1")
    os.chdir(r"./report1")
    os.mkdir(report_folder)
    time.sleep(0.8)
    os.chdir(r"..")
    # os.system("allure generate ./report1 -o ./report1/html/ --clean")
    # os.system("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))
    print("report_folder: ",report_folder)
    os.popen("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))   #成功
    """


class Test_Baidu:
    status ="Pending Approval"


    def setup_class(self):
        self.driver = driver_init()
        self.base = Base(self.driver)
        self.baidu  = Page_Object(self.driver).get_baidu()
        # temp.remove_report_folder()
        # temp.remove_json_file()

    @classmethod
    def teardown_class(self):
        self.driver.quit()
        print("Finish TEST！！！！！！！")
        # self.generate_report()


    def generate_report(self):
        time.sleep(2)
        now_str=time.strftime("%Y_%m_%d_%H_%M_%S",time.localtime(time.time()))
        report_folder = "report_"+now_str
        time.sleep(5)
        os.system("cd C:/Users/jeff.xie/PycharmProjects/MyProject/po_frame/report1")
        os.chdir(r"./report1")
        os.mkdir(report_folder)
        time.sleep(0.8)
        os.chdir(r"..")
        # os.system("allure generate ./report1 -o ./report1/html/ --clean")
        # os.system("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))
        print("report_folder: ",report_folder)
        os.popen("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))   #成功





    @pytest.fixture
    def get_baidu_obj(self):
        print("OKOKOKOKOK")

    @pytest.fixture
    def get_data(self):
        return ["OC","js","php"]


    def test_1(self):
        self.baidu.open_url()


    # blocker：阻塞缺陷（功能未实现，无法下一步）
    # critical：严重缺陷（功能点缺失）
    # normal： 一般缺陷（边界情况，格式错误）
    # minor：次要缺陷（界面错误与ui需求不符）
    # trivial： 轻微缺陷（必须项无提示，或者提示不规范）
    @allure.step(title="")
    @allure.severity(allure.severity_level.BLOCKER)#CRITICAL,NORMAL,MINOR,TRIVIAL
    @allure.issue("https://www.baidu.com")
    def test_2(self):
        allure.attach("描述","测试步骤002的描述...")
        # print("222222222")

    @pytest.mark.run(order=3)
    @pytest.mark.skip(reason='跳过一个方法或一个测试用例')
    @allure.severity(allure.severity_level.NORMAL)
    def test_3(self):
        print("3333333")


    @pytest.mark.skipif(condition="Pending Approval" not in status, reason="满足条件，跳过")
    @allure.testcase("https://www.baidu.com222")
    def test_4(self):
        print("444444")

    @pytest.mark.skipif("Pending Approval" == status, reason="满足条件，跳过")
    def test_5(self):
        allure.attach('在fixture前置操作里面添加一个附件txt: '+"test5", 'fixture前置附件', allure.attachment_type.TEXT)
        print("555555")


    @allure.step(title="test6")
    @allure.severity(allure.severity_level.CRITICAL)#CRITICAL,NORMAL,MINOR,TRIVIAL
    def test_6(self):
        # allure.attach.file("./C.jpg", attachment_type=allure.attachment_type.JPG)
        # ./C.jpg  这个相对路径是putest.ini的相对路径
        allure.attach('<head></head><body> 一个HTML页面 </body>', 'Attach with HTML type', allure.attachment_type.HTML)
        print("666666")
        # allure.name("test_name")


    #这里是一个测试用例
    @pytest.mark.usefixture()   #每一次执行这个方法前，都会去执行get_data方法
    def test_search(self,get_data):
        for i in get_data:
            self.baidu.search_item(i)

    @pytest.mark.run(order=10)
    def test_7(self):
        print("77777777777")

    #这种方式的写法，这里是3个测试用例
    # allure的用法： https://blog.csdn.net/NoamaNelson/article/details/105676063
    # allure.attach的用法： https://www.cnblogs.com/lwjnicole/p/14471484.html
    @pytest.mark.parametrize("item",["Java","Python","C"])
    def test_search_2(self,item):
        try:
            self.baidu.search_item(item)
        except:
            self.attach_picture(item)
            # pic_name =item+ '.png'
            # pic_file = "./screenshot/"+pic_name
            # screenshot.insert_img(self.driver, pic_file)
            # sleep(0.2)
            # allure.attach.file(pic_file,pic_name, attachment_type=allure.attachment_type.PNG)
            assert  3==4


    def test_fail(self):
        """将该用例标记成xfail失败，并且该用例中的后续代码不会执行"""
        pytest.xfail(reason='该功能尚未完成')

    def attach_picture(self,data):
        pic_name =data+ '.png'
        pic_file = "./screenshot/"+pic_name
        screenshot.insert_img(self.driver, pic_file)
        sleep(0.2)
        allure.attach.file(pic_file,pic_name, attachment_type=allure.attachment_type.PNG)

