__author__ = 'Jeff.xie'

import pytest
from base.driver_init import driver_init
from page.page import Page_Object

class Test_loan_sit:

    def setup_class(self):
        self.driver =driver_init()
        self.loan_sit=Page_Object(self.driver).get_loan_sit()
        self.loan_sit.driver.maximize_window()
    # def teardown_class(self):
    #     self.driver.quit()

    @pytest.mark.run(order=1)
    def test_1(self):
        self.loan_sit.open_url()
        self.loan_sit.click_gao()  # 点击高级

    @pytest.mark.run(order=2)
    @pytest.mark.parametrize("username,pw",[("checker_one","Password@123"),("checker_one_d","Password@123")])
    def test_2(self,username,pw):
        self.loan_sit.login(username,pw)
        self.loan_sit.logout()


