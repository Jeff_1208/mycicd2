#! /usr/bin/env pyhton
# -*- coding:utf-8 -*-
# author:jeff.xie
# datetime:2024/1/29 17:14
# software:PyCharm
import allure
from ..page.weather import Weather


class Test_weather():

    w = None

    def setup_class(self):
        self.w = Weather()
        print("start to test")

    def teardown_class(self):
        print("end to test")


    @allure.step(title="获取上海天气")
    @allure.severity(allure.severity_level.TRIVIAL)#CRITICAL,NORMAL,MINOR,TRIVIAL
    @allure.issue("https://www.baidu.com")
    @allure.title("获取上海天气")   #添加这个参数，就会展示在报告中，不添加则默认显示方法名test_05，添加后显示获取上海天气
    def test_05(self):
        r = self.w.get_weather("上海")
        value = self.w.read_json(r.text, "reason")
        print(value)


    @allure.step(title="获取北京天气")
    @allure.severity(allure.severity_level.TRIVIAL)#CRITICAL,NORMAL,MINOR,TRIVIAL
    @allure.issue("https://www.baidu.com")
    @allure.title("获取北京天气")
    def test_06(self):
        r = self.w.get_weather("北京")
        value = self.w.read_json(r.text, "reason")
        print(value)

    @allure.step(title="获取长沙天气")
    @allure.severity(allure.severity_level.TRIVIAL)#CRITICAL,NORMAL,MINOR,TRIVIAL
    @allure.issue("https://www.baidu.com")
    @allure.title("获取长沙天气")
    def test_07(self):
        r = self.w.get_weather("长沙")
        value = self.w.read_json(r.text, "reason")
        print(value)

