__author__ = 'Jeff.xie'


import requests
import json
from public.log import Log

log = Log()

class ApiRequest(object):

    def api_request(self,url,headers,method,data,response_code,response_data_type):

        if method=='get':
            res=requests.get(url,headers=headers,data=data)

        if method=='post':
            res=requests.post(url,headers=headers,data=data)

        if response_data_type == "json":
            code_value  = self.read_json(res.json())


    def read_json(self,json_string):
        dict_all={}
        a = json.dumps(json_string, sort_keys=True)
        b = json.loads(a)
        c = b.items()
        for key, v in c:
            dict_all[key] = v
            print('%s %s' % (key, v))
            if type(v) == dict:
                self.read_json(v)
        return dict_all

    def get_value_from_json(self,json_string,flag):
        a = json.dumps(json_string, sort_keys=True)
        b = json.loads(a)
        c = b.items()
        for key, v in c:
            if key.strip() ==flag:
                return v
            if type(v) == dict:
                self.read_json(v)




