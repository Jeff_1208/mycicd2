__author__ = 'Jeff.xie'



import re
import time
import requests


def read_params():
    params_map={}
    with open('./case/params.txt',"r") as f:
        datas = f.readlines()
        for d in datas:
            d= d.strip().replace("\n","")
            ss= d.split(":")
            params_map[ss[0].strip()] =ss[1].strip()
    return params_map



def update_params(data):
    params_map=read_params()
    params=re.findall(r'{.*?}',data)
    for param in params:
        v = get_param(params_map,param)
        data = data.replace(param,str(v))
    print(data)
    return data

def get_param(params_map,param):
    p = param[1:-1]
    v = params_map.get(p)
    if v == None:
        print(" the param {} is not exist in params.txt".format(param))
        v= "empty"
        if param == '{time_now}':
            v = int(time.time())
    # print("update param {} success".format(param))
    return v



data ='/v1/clock-skew?time={time_now}&token={token}&account={account}'
update_params(data)




# 1 /v1/clock-skew?time={time}	https://mapi.sta-wlab.com	/v1/clock-skew?time={time_now}	GET	{"Content-Type":"application/json","Client-Id":"AHNxFv0FE61r8zfv8kemGwL06LNxS6aM","Client-Secret":"fA52Bm6NkSGGPFPp"}



url ='https://mapi.sta-wlab.com/v1/clock-skew?time={time_now}'

a= "https://mapi.sta-wlab.com"
b="v1/clock-skew?time={time_now}"
import os
url = os.path.join(a,b)
print(url)

print(os.path.join("aaaa",'bbb'))