__author__ = 'Jeff.xie'

import openpyxl


class Data():

    def __init__(self):
          pass


    def read_loans_numbers(self):
        with open('./data/Loans_ID_Stage.txt', 'r') as f:
            data = f.readlines()
        loans_numbers = []
        for i in data:
            if len(i)<10:
                continue
            c = i.replace('\n', '')
            d = c.replace(' ', '')
            loans_numbers.append(d)
        print(loans_numbers)
        return loans_numbers

    def read_xlsx_data(self):
        path ='./tcData.xlsx'
        wk =openpyxl.load_workbook(path)
        mailsheet=wk['mail']
        # print(mailsheet[1][1])
        row2=[item.value for item in list(mailsheet.rows)[2]]
        print('第3行值',row2)
        # print(os.path.split(path)[0])#获取字符串中的文件夹绝对路径
        # print(os.path.split(path)[1])#获取字符串中的文件名

if __name__ == '__main__':
    d= Data()
    d.read_xlsx_data()
    # print(d.read_loans_numbers())